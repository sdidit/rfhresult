from datetime import tzinfo, timedelta, datetime, date

INVALID_TIME = -99999999

SECS_IN_MINUTE = 60
MINS_IN_HOUR = 60
HOURS_IN_DAY = 24
SECS_IN_HOUR = SECS_IN_MINUTE * MINS_IN_HOUR
SECS_IN_DAY = SECS_IN_HOUR * HOURS_IN_DAY


def format_date(d):
    return d.strftime("%d-%b-%Y") if d else '-'


def format_date_short(d):
    return d.strftime("%d/%m") if d else '-'


def format_date_iso(d):
    return d.strftime('%Y-%m-%dT00:00:00') if d else '1970-01-01T00:00:00'

def format_time(t):
    # 123:45.67
    if t == INVALID_TIME:
        return '--:--.--'
    return '%02d:%02d.%02d' % (t / 6000, (t % 6000) / 100, t % 100)


def format_time_short(t):
    # 123:45 for 2h03m45.67s (>= 10m)
    # 123.45 for    2m03.56s (<  10m)
    if t == INVALID_TIME:
        return '--:--'
    if t < 100 * SECS_IN_MINUTE * 10:
        return '%3d.%02d' % (t / 100, t % 100)
    else:
        t += 50
        return '%02d:%02d' % (t / 6000, (t % 6000) / 100)


def format_diff_time(t):
    s = format_time(t)
    if t >= 0:
        s = '+' + s
    return s


class TimeZone(tzinfo):
    def __init__(self, offset, dst_offset, name, *args, **kwargs):
        super(TimeZone, self).__init__(*args, **kwargs)
        self.offset = offset
        self.dst_offset = dst_offset
        self.name = name

    def utcoffset(self, dt):
        return timedelta(hours=self.offset) + self.dst(dt)

    def dst(self, dt):
        return timedelta(hours=self.dst_offset)

    def tzname(self, dt):
        return self.name

    def __str__(self):
        return self.tzname(None)


CET = TimeZone(1, 0, 'CET')
CEST = TimeZone(1, 1, 'CEST')

ACTIVE_TIMEZONE = CEST


def now():
    return datetime.now(ACTIVE_TIMEZONE)


def today():
    return now().date()


def yesterday():
    return today() + timedelta(days=-1)


def now_str():
    return now().ctime() + ' ' + str(ACTIVE_TIMEZONE)


def occurred(date, offset_days):
    return date <= today() + timedelta(days=offset_days)


def date_offset(date, offset_days):
    return date + timedelta(days=offset_days)


def iso_year_start(iso_year):
    """The gregorian calendar date of the first day of the given ISO year"""
    fourth_jan = date(iso_year, 1, 4)
    delta = timedelta(fourth_jan.isoweekday() - 1)
    return fourth_jan - delta


def iso_to_gregorian(iso_year, iso_week, iso_day):
    """Gregorian calendar date for the given ISO year, week and day"""
    year_start = iso_year_start(iso_year)
    return year_start + timedelta(iso_day - 1, 0, 0, 0, 0, 0, iso_week - 1)


def date_to_iso_week(date):
    return date.isocalendar()[1]


def week_boundaries(iso_year, iso_week):
    mon = iso_to_gregorian(iso_year, iso_week, 1)
    sun = mon + timedelta(days=6)
    return mon, sun
