#!/usr/bin/env python

import webapp2
from settings import DEBUG
import views

app = webapp2.WSGIApplication(
    [
        ('/', views.MainHandler),
        ('/rally/(.*)/results/csv/(.*)', views.CSVRallyResultsHandler),
        ('/rally/(.*)/results/', views.RallyResultsHandler),
        ('/rally/(.*)/prov_results/', views.RallyLiveResultsHandler),
        ('/rally/(.*)/image/', views.RallyImageHandler),
        ('/rally/(.*)/csv/(.*)', views.CSVRallyHandler),
        ('/rally/(.*)/', views.RallyHandler),
        ('/champ/xml/', views.XMLChampListHandler),
        ('/champ/', views.ChampListHandler),
        ('/champ/(.*)/drivers/', views.ChampDriversHandler),
        ('/champ/(.*)/results/', views.ChampResultsHandler),
        ('/champ/(.*)/results/xml/', views.XMLChampResultsHandler),
        ('/champ/(.*)/xml/(.*)', views.XMLChampStandingsHandler),
        ('/champ/(.*)/', views.ChampStandingsHandler),
        ('/teamcup/standings/', views.TeamCupStandingsHandler),
        ('/member/', views.MemberListHandler),
        ('/member/(.*)/', views.MemberHandler),
        ('/team/', views.TeamListHandler),
        ('/team/(.*)/image/', views.TeamBannerHandler),
        ('/rank/stage/(.*)/', views.StageRankHandler),
        ('/schedule/', views.ScheduleHandler),
        ('/tools/', views.ToolsHandler),
        ('/tools/roadbook/csv/', views.ToolsRoadbookCSVHandler),
        ('/mijnrally/schedule/', views.MijnRallyScheduleHandler),
        ('/mijnrally/upcoming/', views.MijnRallyUpcomingRalliesHandler),
        ('/mijnrally/rally/results/top/', views.MijnRallyRallyTopResultsHandler),
        ('/mijnrally/rally/(.*)/results/top/', views.MijnRallyRallyTopResultsHandler),
        ('/mijnrally/champ/(.*)/top/', views.MijnRallyChampTopStandingsHandler),
        ('/mijnrally/teamcup/top/', views.MijnRallyTeamCupTopStandingsHandler),
        ('/login/', views.LoginHandler), # unused
        ('/logout/', views.LogoutHandler), # unused
        ('/.*', views.PageNotFoundHandler),
    ], debug=DEBUG)
