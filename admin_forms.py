#from google.appengine.ext.db import djangoforms
from django.forms.util import ValidationError

import djangoforms

#from wtforms.ext.appengine.db import model_form
#from wtforms.validators import ValidationError
import models


def already_exists(found_obj, edit_obj):
    # if found then already exists if not same obj
    return found_obj is not None and (edit_obj is None or edit_obj.is_saved() and found_obj.key() != edit_obj.key())


##class ResultManagerForm(djangoforms.ModelForm):
##    class Meta:
##        model = models.ResultManager
##
##    def clean(self):
##        obj = models.ResultManager.all().filter('user = ', self.cleaned_data['user']).get()
##        if already_exists(obj, self.instance):
##            raise ValidationError('A result manager with this account already exists')
##        return self.cleaned_data


#class MemberForm(model_form(models.Member)):
#def validate_name(form, field):
#name = field.data
#if name:
#obj = models.Member.all().filter('name = ', name).get()
#if already_exists(obj, self.instance):
#raise ValidationError('A member with this name already exists')

class MemberForm(djangoforms.ModelForm):
    class Meta:
        model = models.Member

    def clean(self):
        name = self.cleaned_data.get('name')
        if name:
            obj = models.Member.all().filter('name = ', name).get()
            if already_exists(obj, self.instance):
                raise ValidationError('A member with this name already exists')
        return self.cleaned_data


class TeamForm(djangoforms.ModelForm):
    class Meta:
        model = models.Team

    def clean(self):
        name = self.cleaned_data.get('name')
        if name:
            obj = models.Team.all().filter('name = ', name).get()
            if already_exists(obj, self.instance):
                raise ValidationError('A team with this name already exists')
        return self.cleaned_data


class TeamMemberForm(djangoforms.ModelForm):
    class Meta:
        model = models.TeamMember
        exclude = ['team']

    def clean(self):
        member = self.cleaned_data.get('member')
        if not member:
            raise ValidationError('Member field is required')
        return self.cleaned_data


class TeamCupForm(djangoforms.ModelForm):
    class Meta:
        model = models.TeamCup

    def clean(self):
        name = self.cleaned_data.get('name')
        if name:
            obj = models.TeamCup.all().filter('name = ', name).get()
            if already_exists(obj, self.instance):
                raise ValidationError('A team cup with this name already exists')
        return self.cleaned_data


class ChampForm(djangoforms.ModelForm):
    class Meta:
        model = models.Champ
        exclude = ['rules_link']

    def clean(self):
        name = self.cleaned_data.get('name')
        if name:
            obj = models.Champ.all().filter('name = ', name).get()
            if already_exists(obj, self.instance):
                raise ValidationError('A championship with this name already exists')
        return self.cleaned_data


class DriverForm(djangoforms.ModelForm):
    class Meta:
        model = models.Driver
        exclude = ['champ', 'car']

    def clean(self):
        member = self.cleaned_data.get('member')
        if not member:
            raise ValidationError('Member field is required')
        champ = self.instance.champ
        ##        obj = champ.driver_set.filter('member = ', member).get()
        ##        if already_exists(obj, self.instance):
        ##            raise ValidationError('A driver for this member already exists')
        obj = champ.driver_set.filter('plugin_id = ', self.cleaned_data['plugin_id']).get()
        if already_exists(obj, self.instance):
            raise ValidationError('A driver with this plugin id already exists')
        car_class = self.cleaned_data['car_class']
        if not champ.is_car_class_allowed(car_class):
            raise ValidationError('Car class %s not allowed in this championship' % car_class)
        return self.cleaned_data


class RallyForm(djangoforms.ModelForm):
    class Meta:
        model = models.Rally
        exclude = ['champ', 'imported_results']

    def clean(self):
        name = self.cleaned_data.get('name')
        if not name:
            raise ValidationError('Name field is required')
        champ = self.instance.champ
        obj = champ.rally_set.filter('name = ', name).get()
        if already_exists(obj, self.instance):
            raise ValidationError('A rally with this name already exists')
        sdate = self.cleaned_data.get('start_date')
        edate = self.cleaned_data.get('end_date')
        if not sdate or not edate:
            raise ValidationError('Start and end date fields are required (format yyyy-mm-dd)')
        if sdate > edate:
            raise ValidationError('Start date must lie before end date')
        count = self.cleaned_data['stage_count']
        if count > 50:
            raise ValidationError('Stage count (%d) too high (max = 50)' % count)
            #names = self.cleaned_data.get('stage_names')
            #names_count = len(names.splitlines()) if names else 0
            #if names_count and names_count != count:
            #raise ValidationError('Stage count (%d) does not match number of stage names (%d)' % (count, names_count))
        return self.cleaned_data


class StageForm(djangoforms.ModelForm):
    class Meta:
        model = models.Stage
        exclude = ['nr', 'rally']


class NewsItemForm(djangoforms.ModelForm):
    class Meta:
        model = models.NewsItem


