import re
import urllib2
import logging
import cPickle
import zlib
import itertools

from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.api import images
import settings

import time_util
import tools
import charts


ALL_CLASSES = (
    'A8W-14', 'A8W-13', 'A8W-12', 'A8W-11', 'A8W',
    'A8', 'S2000', 'RRC', 'R3', 'A7', 'R2', 'A6', 'A5',
    'N4', 'N3', 'N2', 'N1',
    'GT', 'VH', 'Group_H', 'Group_B')

CLASS_FACTORS = {}  # 'A8': 0.75, 'A7': 0.9, 'S2': 0.9, 'N4': 0.9}

ALL_COUNTRIES = (
    '-', 'ad', 'ar', 'au', 'bb', 'be', 'bg', 'br', 'ca', 'ch', 'co', 'cr', 'ct', 'cz', 'de', 'dk', 'ec', 'ee', 'es',
    'fi', 'fr', 'gr', 'hr', 'hu', 'il', 'ir', 'it', 'jm', 'lk', 'lt', 'lv', 'mg', 'mq', 'mx', 'nl', 'no', 'ph', 'pl',
    'pt', 'py', 'ro', 'ru', 'PLUGINSse', 'si', 'tr', 'uk', 'us')

ALL_PLUGINS = ('RSRBR', 'RBRTM', 'RSFHU')
ALL_PLUGIN_VERSIONS = ('2010', '2011', '2012', '2013', '2014', '2015')
ALL_CHAMP_TYPES = ('Official', 'Unofficial', 'Public')
ALL_CHAMP_VISIBILITIES = ('Hidden', 'Archived', 'Visible')
ALL_POINTS_SYSTEMS = ('KNAF2009', 'KNAF2010', 'KNAF2011', 'WRC2011', 'CHALLENGE', 'MDO2013')
RESULT_TYPES = ('Best', 'First', 'Last')

RESULT_STATES = ('Finished', 'Retired', 'Skipped', 'In progress')
STATE_FINISHED = 0
STATE_RETIRED = 1
STATE_SKIPPED = 2
STATE_IN_PROGRESS = 3

FILTER_ALL = {'value': 0, 'name': 'All'}
FILTER_RFH = {'value': 1, 'name': 'mijnrally.nl'}
ALL_FILTERS = (FILTER_ALL, FILTER_RFH)

MAX_TEAM_MEMBERS = 2
TEAM_IMAGE_WIDTH = 500
TEAM_IMAGE_HEIGHT = 100
TEAM_IMAGE_TYPE = images.PNG
TEAM_IMAGE_CONTENT_TYPE = "image/png"

RALLY_IMAGE_WIDTH = 350
RALLY_IMAGE_TYPE = images.PNG
RALLY_IMAGE_CONTENT_TYPE = "image/png"

DEFAULT_ROADBOOK_PUBLICATION_DAYS = 10
DEFAULT_WEATHER_FORECAST_DAYS = 0


class MyStringListProperty(db.StringListProperty):
    def __init__(self, *args, **kwargs):
        super(MyStringListProperty, self).__init__(*args, **kwargs)
        # allow an empty list: djangoforms.StringListProperty's make_value_from_form() can handle an
        # empty value, returning an empty list; leaving as-is is rejected in CharField's clean()
        self.required = False


class PointsSystem(object):
    def points(self, pos):
        return 0

    def _points(self, pos, p, rest):
        return p[pos - 1] if 1 <= pos <= len(p) else rest


class HalfPointsSystem(PointsSystem):
    def __init__(self, ps):
        super(HalfPointsSystem, self).__init__()
        self.ps = ps

    def points(self, pos):
        return tools.round_up(self.ps.points(pos) / 2.0)


class KNAF2009PointsSystem(PointsSystem):
    p = range(100, 1, -2)

    def points(self, pos):
        return self._points(pos, self.p, 0)


class KNAF2010PointsSystem(PointsSystem):
    p = [100, 80, 65, 55, 45, 35, 30, 25, 20, 18, 16, 14, 12, 10, 8, 6, 5, 4, 3, 2]

    def points(self, pos):
        return self._points(pos, self.p, 1)


class KNAF2011PointsSystem(PointsSystem):
    p = [50, 42, 35] + range(32, 1, -2)

    def points(self, pos):
        return self._points(pos, self.p, 0)


class WRC2011PointsSystem(PointsSystem):
    p = [25, 18, 15, 12, 10, 8, 6, 4, 2, 1]

    def points(self, pos):
        return self._points(pos, self.p, 0)


class ChallengePointsSystem(PointsSystem):
    p = range(10, 0, -1)

    def points(self, pos):
        return self._points(pos, self.p, 0)


class MDO2013PointsSystem(PointsSystem):
    p = [25, 22, 20, 18] + range(16, 0, -1)

    def points(self, pos):
        return self._points(pos, self.p, 0)


PointsSystemMap = {
    'Empty': PointsSystem(),
    'KNAF2009': KNAF2009PointsSystem(),
    'KNAF2010': KNAF2010PointsSystem(),
    'KNAF2011': KNAF2011PointsSystem(),
    'WRC2011': WRC2011PointsSystem(),
    'CHALLENGE': ChallengePointsSystem(),
    'MDO2013': MDO2013PointsSystem(),
}


def to_blob(obj):
    """Turn an object into a compressed pickled blob"""
    return db.Blob(zlib.compress(cPickle.dumps(obj)))


def from_blob(obj):
    """Turn a compressed pickled blob into an object"""
    return cPickle.loads(zlib.decompress(obj))


class NamedKey(object):
    def __init__(self, key, name):
        self.key = key
        self.name = name or ''

    def __eq__(self, other):
        return self.key == other.key

    def __ne__(self, other):
        return self.key != other.key

    def __hash__(self):
        return hash(self.key)

    def __str__(self):
        return unicode(self).encode(settings.DEFAULT_CHARSET)

    def __unicode__(self):
        return self.name


class Result(object):
    def __init__(self, plugin_id, name, country, car_class, car, t1, t2, t3, session=0, status=STATE_FINISHED):
        if not country in ALL_COUNTRIES:
            country = '-'
        self.country = country
        self.name = name
        self.plugin_id = plugin_id
        self.car_class = car_class
        self.car = car
        self.t3 = t3
        self.session = session
        self.status = status if 0 <= status < len(RESULT_STATES) else STATE_FINISHED

        # stage result only
        self.t1 = t1
        self.t2 = t2

        # overall result only
        self.presence_bonus = 0
        self.finish_bonus = 0
        self.most_wins_bonus = 0
        self.finishes = 0
        self.wins = 0

        # set later
        self.member_name = ''
        self.team = ''

    def from_driver(self, driver):
        assert driver is None or isinstance(driver, Driver)
        self.member_name = driver.member.name
        ##        # change name to member name
        ##        if filtered:
        ##            self.name = driver.member.name
        ##        elif self.name != driver.member.name:
        ##            self.name += ' (%s)' % driver.member.name
        self.team = driver.get_team_name()
        # if no car class in result, get a default from driver
        if not self.car_class:
            self.car_class = driver.car_class

    def calc_points(self, first, prev, points_system):
        # calculate diff to first, diff to prev, position and points
        # position will be equal to previous if diff to previous is zero
        initial = self == first
        self.index = 0 if initial else prev.index + 1
        self.diff_first = self.t3 - first.t3
        self.diff_prev = self.t3 - prev.t3   # if initial then self, first and prev will all be the same
        if self.diff_prev or initial:
            self.same_as_previous = False
            self.pos = self.index + 1
            self.points = points_system.points(self.pos)
        else:
            self.same_as_previous = True
            self.pos = prev.pos
            self.points = prev.points

        # must return self to work with reduce, will be passed as prev in next call
        return self

    def split1(self):
        return time_util.format_time(self.t1)

    def split2(self):
        return time_util.format_time(self.t2)

    def time(self):
        return time_util.format_time(self.t3)

    def time_short(self):
        return time_util.format_time_short(self.t3)

    def total_points(self):
        # no points if DNF
        points = self.points if self.status == STATE_FINISHED else 0
        return points + self.presence_bonus + self.finish_bonus + self.most_wins_bonus

    def team_points(self):
        points = self.total_points()
        cls = self.car_class
        factor = CLASS_FACTORS.get(cls[:2])
        #return tools.round_up(factor * points) if factor else points
        return round(factor * points, 2) if factor else points

    def status_str(self):
        return RESULT_STATES[self.status]

    def is_finished(self):
        return self.status == STATE_FINISHED

    def pos_str(self):
        return '%d%s' % (self.pos, '=' if self.same_as_previous else '.')

    def diff_first_str(self):
        return time_util.format_diff_time(self.diff_first)

    def diff_prev_str(self):
        return time_util.format_diff_time(self.diff_prev)

    def __cmp__(self, r):
        return \
            cmp(self.status, r.status) or \
            cmp(self.t3, r.t3) or \
            cmp(self.t2, r.t2) or \
            cmp(self.t1, r.t1) or \
            cmp(self.name, r.name)

    def csv_values(self):
        return self.name, self.plugin_id, self.country, self.car_class, self.car, self.time(),\
            self.diff_first_str(), self.diff_prev_str()


class MemberResult(object):
    def __init__(self, member, points):
        self.member = member
        self.points = points

    def add(self, mr):
        self.points += mr.points

    def __str__(self):
        return unicode(self).encode(settings.DEFAULT_CHARSET)

    def __unicode__(self):
        return unicode(self.member)

    def __cmp__(self, r):
        c = tools.signum(r.points - self.points)
        if not c:
            c = cmp(self.member.name, r.member.name)
        return c


class TeamResult(object):
    def __init__(self, team, driver_results=None, plugin_ids=None, bonus=None):
        self.team = team
        driver_results = driver_results or []
        plugin_ids = plugin_ids or {}

        def make_entry(r):
            member = plugin_ids[r.plugin_id].member
            member_result = MemberResult(member.named_key(), r.team_points())
            return member.key(), member_result

        self.member_result_dict = dict(make_entry(r) for r in driver_results)
        self.points = sum(mr.points for mr in self.member_result_dict.values())
        if bonus:
            bonus_index = len(plugin_ids) - 1
            if 0 <= bonus_index < len(bonus):
                bonus_points = bonus[bonus_index]
                if isinstance(bonus_points, float):
                    self.points += tools.round_up(float(self.points) * bonus_points)
                else:
                    self.points += bonus_points

    def member_results(self):
        return sorted(self.member_result_dict.values())

    def is_multi_driver(self):
        return len(self.member_result_dict) > 1

    def has_points(self):
        return self.points > 0

    def __cmp__(self, r):
        c = tools.signum(r.points - self.points)
        if not c:
            c = cmp(self.team.name, r.team.name)
        return c


class TeamStanding(TeamResult):
    def __init__(self, team, count):
        super(TeamStanding, self).__init__(team)
        self.rally_results = [None] * count

    def add(self, tr, index):
        self.rally_results[index] = tr

        # total team points
        self.points += tr.points

        # total team points for each member
        for k, omr in tr.member_result_dict.items():
            mr = self.member_result_dict.get(k)
            if mr is None:
                self.member_result_dict[k] = MemberResult(omr.member, omr.points)
            else:
                mr.add(omr)


class RoadbookStage(object):
    def __init__(self, stage_name, rbr_stage_name, mod, weather, tyre_change, time_of_day, nr_in_leg, wear=u'New',
                 grip=u'Dry'):
        self.stage_name = stage_name
        self.rbr_stage_name = rbr_stage_name
        self.mod = mod
        self.weather = weather
        self.wear = wear
        self.grip = grip
        self.tyre_change = tyre_change
        self.time_of_day = time_of_day
        self.nr_in_leg = nr_in_leg

    def csv_values(self):
        return [str(self.nr_in_leg), str(self.tyre_change), self.stage_name or '', self.rbr_stage_name or '',
                self.mod or '',
                self.weather or 'TBA', self.wear or 'TBA', self.grip or 'TBA', self.time_of_day]


class Roadbook(object):
    def __init__(self, name, sdate, edate, img_link, stages):
        self.name = name
        self.sdate = sdate
        self.edate = edate
        self.img_link = img_link
        self.stages = stages

    def csv_values(self):
        sdate = time_util.format_date(self.sdate)
        edate = time_util.format_date(self.edate)
        stages = map(RoadbookStage.csv_values, self.stages)
        for i, stage in enumerate(stages):
            stage.insert(0, str(i + 1))
        comment = '; Roadbook of %s from %s to %s' % (self.name, sdate, edate)
        header = ['nr', 'in_leg', 'repair', 'stage_name', 'rbr_name', 'mod', 'weather', 'wear', 'grip', 'time_of_day']
        return comment, header, stages


class MyModel(db.Model):
    def named_key(self):
        return NamedKey(self.key(), unicode(self))

    def __str__(self):
        return unicode(self).encode(settings.DEFAULT_CHARSET)


class ResultManager(MyModel):
    user = db.UserProperty(required=True)

    @classmethod
    def is_current_user_result_manager(cls):
        return False  # cls.all().filter('user', users.get_current_user()).get() is not None

    def __unicode__(self):
        return self.user


# noinspection PyUnresolvedReferences
class Member(MyModel):
    name = db.StringProperty(required=True)
    country = db.StringProperty(required=True, choices=ALL_COUNTRIES, default='nl')
    email_address = db.EmailProperty()
    rsrbr_id = db.StringProperty()
    rbrtm_id = db.StringProperty()
    rallysimfans_id = db.StringProperty()
    retired = db.BooleanProperty(default=False)

    def delete(self):
        for driver in self.driver_set:
            driver.delete()
        for team_member in self.teammember_set:
            team_member.delete()
        super(Member, self).delete()

    def team(self):
        today = time_util.today()
        m = filter(lambda tm: tm.is_current(today), self.teammember_set)
        return m and m[0].team

    def active_as(self):
        return filter(lambda d: d.champ.is_non_hidden(), self.driver_set.filter('active', True))

    def wants_notifications(self):
        return not self.retired and self.email_address

    def __unicode__(self):
        return self.name


# noinspection PyUnresolvedReferences
class Team(MyModel):
    name = db.StringProperty(required=True)
    banner = db.BlobProperty()

    def import_banner(self, data=None):
        try:
            if data:
                image = images.Image(data)
                image.resize(TEAM_IMAGE_WIDTH, TEAM_IMAGE_HEIGHT)
                self.banner = db.Blob(image.execute_transforms(TEAM_IMAGE_TYPE))
                return self.banner
        except Exception, e:
            logging.exception('Could not import banner: %s', e)
        return None

    def members(self, date=None):
        if date is None:
            date = time_util.today()
        members = map(TeamMember.get_member, filter(lambda tm: tm.is_current(date), self.teammember_set))
        return members[:MAX_TEAM_MEMBERS]

    def drivers(self, rally):
        member_keys = [m.key() for m in self.members(rally.end_date)]
        all_drivers = set(rally.participants())
        return filter(lambda d: d.member.key() in member_keys, all_drivers)

    def driver_map(self, rally):
        """dict of plugin id to Driver"""
        drivers = self.drivers(rally)
        return dict((d.plugin_id, d) for d in drivers)

    def __unicode__(self):
        return self.name


class TeamMember(MyModel):
    team = db.ReferenceProperty(Team, required=True)
    member = db.ReferenceProperty(Member)
    start_date = db.DateProperty()
    end_date = db.DateProperty()

    @classmethod
    def team_changes(cls, sdate, edate):
        joined = cls.all().filter('start_date >=', sdate).filter('start_date <=', edate)
        left = cls.all().filter('end_date >=', sdate).filter('end_date <=', edate)

        def make_result(m, join):
            return dict(joined=join, date=m.start_date if join else m.end_date, member=m.member, team=m.team)
        result = [make_result(m, True) for m in joined] + [make_result(m, False) for m in left]
        return result

    def get_member(self):
        return self.member

    def sdate_str(self):
        return time_util.format_date(self.start_date)

    def edate_str(self):
        return time_util.format_date(self.end_date)

    def period_str(self):
        if self.start_date:
            if self.end_date:
                return '%s to %s' % (self.sdate_str(), self.edate_str())
            else:
                return 'since %s' % self.sdate_str()
        elif self.end_date:
            return 'until %s' % self.edate_str()
        return ''

    def is_current(self, date):
        return (not self.start_date or self.start_date <= date) and (not self.end_date or self.end_date >= date)

    def __unicode__(self):
        return '%s - %s %s' % (self.team.name, self.member.name, self.period_str())


# noinspection PyUnresolvedReferences
class TeamCup(MyModel):
    name = db.StringProperty()
    bonus_for_member_count = MyStringListProperty()

    def __unicode__(self):
        return self.name

    def delete(self):
        for champ in self.champ_set:
            champ.team_cup = None
            champ.put()
        super(TeamCup, self).delete()

    def get_bonus(self):
        def parse_bonus(b):
            if b:
                try:
                    if b.endswith('%'):
                        b = float(b[:-1]) / 100.0
                    else:
                        b = b.replace(',', '.')
                        b = float(b) if '.' in b else int(b)
                except:
                    b = 0
            else:
                b = 0
            return b

        # noinspection PyTypeChecker
        return map(parse_bonus, self.bonus_for_member_count)

    def init_standings(self, standings, rallies):
        self.standings = standings
        self.rallies = rallies

    def create_bar_chart(self):
        lengths = map(lambda tr: len(tr.member_results()), self.standings)
        count = max(lengths) if lengths else 0

        def create_item(tr):
            mrs = tr.member_results()
            points = map(lambda mr: mr.points, mrs)
            diff = count - len(mrs)
            if diff > 0:
                points += [0] * diff
            points += [tr.points - sum(points)]  # last part of the bar for bonus points
            return charts.ChartItem(tr.team.name, points)

        items = map(create_item, self.standings)
        self.chart_url = charts.create_bar_chart(items)

    def create_line_chart(self):
        def create_item(ts):
            cumulative = tools.accumulate(r.points if r else 0 for r in ts.rally_results)
            return charts.ChartItem(ts.team.name, cumulative)

        items = map(create_item, self.standings)
        # all rally labels won't fit, use empty labels
        labels = [''] * len(self.rallies)  # [rally.short_name or '' for rally in self.rallies]
        self.chart2_url = charts.create_line_chart(items, labels)


# noinspection PyUnresolvedReferences
class Champ(MyModel):
    name = db.StringProperty(required=True)
    plugin = db.StringProperty(required=True, choices=ALL_PLUGINS, default='RSRBR')
    plugin_version = db.StringProperty(required=True, choices=ALL_PLUGIN_VERSIONS, default=str(settings.YEAR))
    championship_type = db.StringProperty(required=True, choices=ALL_CHAMP_TYPES, default='Official')
    car_classes = db.StringListProperty(default=ALL_CLASSES)
    result_classes = db.StringListProperty(
        default=('A8W-12', 'A8W+A8=A8W+A8W-11+A8', 'RRC', 'S2000', 'R3', 'N4', 'A7', 'A6'))
    points_system = db.StringProperty(required=True, choices=ALL_POINTS_SYSTEMS, default='WRC2011')
    fixed_car = db.StringProperty()
    relevant_rally_count = db.IntegerProperty()
    registered_drivers_only = db.BooleanProperty(default=True)
    driver_nations = MyStringListProperty()
    team_cup = db.ReferenceProperty(TeamCup)
    roadbook_publication_days = db.IntegerProperty(default=DEFAULT_ROADBOOK_PUBLICATION_DAYS)
    weather_forecast_days = db.IntegerProperty(default=DEFAULT_WEATHER_FORECAST_DAYS)
    external_link = db.LinkProperty()
    rules_link = db.LinkProperty()
    visibility = db.StringProperty(required=True, choices=ALL_CHAMP_VISIBILITIES, default='Visible')
    closed = db.BooleanProperty(default=False)
    minimum_drivers_for_full_points = db.IntegerProperty(default=0)
    bonus_for_start = db.IntegerProperty()
    bonus_for_finish = db.IntegerProperty()
    bonus_for_most_wins = db.IntegerProperty()

    def __init__(self, parent=None, key_name=None, _app=None, _from_entity=False, **kwds):
        super(Champ, self).__init__(parent, key_name, _app, _from_entity, **kwds)
        self.plugin_id_driver = None
        self.driver_cache = None
        self.result_class_dict = None

    @classmethod
    def non_hidden_champs(cls):
        return filter(cls.is_non_hidden, cls.all().order('closed').order('name'))

    @classmethod
    def visible_champs(cls):
        return cls.all().filter('visibility', 'Visible').order('closed').order('name')

    @classmethod
    def rallies_to_be_imported(cls, advance_days=3):
        result = []
        champs = cls.all().filter('closed', False).filter('roadbook_publication_days >', 0)
        for champ in champs:
            result += champ._rallies_to_be_imported(advance_days)
        return result

    def _rallies_to_be_imported(self, advance_days=3):
        offset = self.roadbook_publication_days + advance_days
        today = time_util.today()
        sdate = time_util.date_offset(today, offset)
        rallies = self.rally_set.filter('imported_roadbook', False)
        rallies = rallies.filter('start_date >=', today).filter('start_date <=', sdate)
        return [rally for rally in rallies]

    def get_plugin_name(self):
        return '%s_%s' % (self.plugin, self.plugin_version)

    def rallies(self):
        return self.rally_set.order('start_date')

    def delete(self):
        self.delete_standings()
        for driver in self.driver_set:
            driver.delete()
        for rally in self.rally_set:
            rally.delete()
        super(Champ, self).delete()

    def is_official(self):
        return self.championship_type == 'Official'

    def is_unofficial(self):
        return self.championship_type == 'Unofficial'

    def is_public(self):
        return self.championship_type == 'Public'

    def is_non_hidden(self):
        return self.visibility != 'Hidden'

    def is_show_filter(self):
        return not self.registered_drivers_only and not self.driver_nations

    def has_any_bonus(self):
        return self.bonus_for_start or self.bonus_for_finish or self.bonus_for_most_wins

    def is_car_class_allowed(self, cls):
        return cls in self.car_classes

    def result_class_entries(self):
        def create_entry(cls):
            entry = cls.split('=', 1)
            return entry[0], entry[-1].split('+')

        return [create_entry(cls) for cls in self.result_classes]

    def result_class_names(self):
        return [cls.split('=')[0] for cls in self.result_classes]

    def result_class_to_classes(self, cls):
        if self.result_class_dict is None:
            self.result_class_dict = dict(self.result_class_entries())
        return self.result_class_dict.get(cls) or []

    def is_valid_result_class_name(self, cls):
        return cls in self.result_class_names()

    def active_drivers(self):
        if self.driver_cache is None:
            # lazy init
            self.driver_cache = self.driver_set.filter('active', True).order('plugin_id')
        return self.driver_cache

    def participants(self, date=None, cls=None):
        drivers = self.active_drivers()
        if cls:
            c = set(self.result_class_to_classes(cls))
            drivers = filter(lambda d: d.car_class in c, drivers)
        if date:
            drivers = filter(lambda d: not d.start_date or d.start_date <= date, drivers)
        return drivers

    def plugin_id_driver_map(self):
        if self.plugin_id_driver is None:
            # lazy init
            self.plugin_id_driver = dict([(driver.plugin_id, driver) for driver in self.participants()])
        return self.plugin_id_driver

    def get_driver(self, plugin_id):
        return self.plugin_id_driver_map().get(plugin_id)

    def participant_plugin_ids(self, date=None):
        return set([driver.plugin_id for driver in self.participants(date)])

    def blacklisted_plugin_ids(self):
        return set([driver.plugin_id for driver in self.blacklisteddriver_set])

    def get_blacklisted_driver(self, plugin_id):
        return self.blacklisteddriver_set.filter('plugin_id', plugin_id).get()

    def blacklist(self, plugin_ids):
        for plugin_id in plugin_ids:
            driver = self.get_blacklisted_driver(plugin_id)
            if driver is None:
                driver = BlacklistedDriver(champ=self, plugin_id=plugin_id)
                driver.put()

    def generate_participants(self):
        pidname = self.plugin.lower() + '_id'
        curmemberkeys = [d.member.key() for d in self.active_drivers()]
        memberdict = dict([(d.key(), d) for d in Member.all().filter('retired', False).filter(pidname + ' != ', '')])
        for k in curmemberkeys:
            if k in memberdict:
                del memberdict[k]
        car_class = self.car_classes[0] if self.car_classes else ''
        if not car_class in ALL_CLASSES:
            car_class = car_class[:2]
            if not car_class in ALL_CLASSES:
                car_class = ALL_CLASSES[0]
        drivers = [Driver(champ=self, member=m, plugin_id=getattr(m, pidname), car_class=car_class) for m in
                   memberdict.itervalues()]
        map(Driver.put, drivers)

    def process_results(self, results, flt, cls=None, date=None):
        # result filtering is always done, also on imported results stored in the database

        results = self.filter_results_on_car_class(results, cls)
        if flt and self.is_show_filter():
            results = self.filter_results_on_registered_drivers(results, date)
        results = self.filter_results_on_blacklisted_drivers(results)

        results = sorted(results)
        for result in results:
            assert isinstance(result, Result)
            driver = self.get_driver(result.plugin_id)
            if driver is not None:
                assert isinstance(driver, Driver)
                result.from_driver(driver)
                # if no car in result, use the fixed car
            if not result.car:
                result.car = self.fixed_car or ''

        return results and self._calc_result_points(results)

    def filter_results_on_car_class(self, results, cls):
        if cls:
            c = set(self.result_class_to_classes(cls))
            results = filter(lambda result: result.car_class in c, results)
        return results

    def filter_results_on_registered_drivers(self, results, date):
        plugin_ids = self.participant_plugin_ids(date)
        return filter(lambda result: result.plugin_id in plugin_ids, results)

    def filter_results_on_driver_nations(self, results):
        nations = set(str(nation) for nation in self.driver_nations)
        return filter(lambda result: result.country in nations, results)

    def filter_results_on_blacklisted_drivers(self, results):
        blacklist = self.blacklisted_plugin_ids()
        if blacklist:
            results = filter(lambda result: result.plugin_id not in blacklist, results)
        return results

    def prepare_results_for_import(self, results, rally):
        if self.registered_drivers_only:
            results = self.filter_results_on_registered_drivers(results, rally.end_date)
        if self.driver_nations:
            results = self.filter_results_on_driver_nations(results)
        return results

    def _get_standings_cache(self, cls, flt):
        return self.standingscache_set.filter('cls', cls or '').filter('flt', flt).get()

    def get_standings(self, cls, flt):
        s = self._get_standings_cache(cls, flt)
        return s and s.get_standings() or None

    def set_standings(self, standings, cls, flt):
        if standings:
            s = self._get_standings_cache(cls, flt) or StandingsCache(champ=self, cls=cls or '', flt=flt)
            s.set_standings(standings)
            s.put()

    def delete_standings(self):
        for s in self.standingscache_set:
            s.delete()

    def _calc_result_points(self, results):
        first = results[0]
        ps = PointsSystemMap[self.points_system or 'Empty']
        if len(results) < self.minimum_drivers_for_full_points:
            ps = HalfPointsSystem(ps)
        reduce(lambda prev, result: result.calc_points(first, prev, ps), results, first)
        return results

    def import_rallies(self, rallies_csv):
        this_year = time_util.today().year
        rally = None
        stages = []
        for line in rallies_csv.splitlines():
            columns = line.split(u'|')
            if len(columns) > 0 and columns[0]:
                if rally is None:
                    rally = self.rally_set.filter('import_name =', columns[0]).get()
                    week = int(columns[1])
                    year = this_year
                    if week >= 51:
                        year -= 1
                    sdate, edate = time_util.week_boundaries(year, week)
                    if rally:
                        rally.start_date = sdate
                        rally.end_date = edate
                    else:
                        rally = Rally(champ=self,
                                      name=columns[0],
                                      import_name=columns[0],
                                      start_date=sdate,
                                      end_date=edate)
                elif columns[0] == 'End':
                    if rally:
                        rally.stage_count = len(stages)
                        rally.leg_count = int(stages[rally.stage_count - 1][0]) if stages else 0
                        rally.put()
                    rally = None
                    stages = []
                else:
                    stages.append(line)

    def import_roadbook(self, roadbook):
        rallies = [rally for rally in self.rally_set.filter('name', roadbook.name).fetch(1)]
        if rallies:
            rally = rallies[0]
        else:
            rally = Rally(champ=self)
        rally.import_roadbook(roadbook)
        return rally

    def __unicode__(self):
        return self.name


# noinspection PyUnresolvedReferences
class Driver(MyModel):
    member = db.ReferenceProperty(Member)
    champ = db.ReferenceProperty(Champ, required=True)
    active = db.BooleanProperty(default=True)
    start_date = db.DateProperty(auto_now_add=False)
    plugin_id = db.StringProperty()
    car_class = db.StringProperty(required=True, choices=ALL_CLASSES, default='A8')
    car = db.StringProperty()

    def get_member(self):
        return self.member

    def get_plugin_id(self):
        return self.plugin_id

    def sdate_str(self):
        return time_util.format_date(self.start_date)

    def sdate_short_str(self):
        return time_util.format_date_short(self.start_date)

    def get_team_name(self):
        team = self.member.team()
        return team.name if team else ''

    def __unicode__(self):
        if self.member:
            return u'%s (%s)' % (self.plugin_id, unicode(self.member))
        return self.plugin_id


class BlacklistedDriver(MyModel):
    champ = db.ReferenceProperty(Champ, required=True)
    plugin_id = db.StringProperty()

    def __unicode__(self):
        return self.plugin_id


class StandingsCache(MyModel):
    champ = db.ReferenceProperty(Champ, required=True)
    flt = db.IntegerProperty(required=True, default=0)
    cls = db.StringProperty(default='')
    standings = db.BlobProperty()  # compressed pickled list of DriverStandings

    def set_standings(self, standings):
        self.standings = db.Blob(zlib.compress(cPickle.dumps(standings)))

    def get_standings(self):
        try:
            return cPickle.loads(zlib.decompress(self.standings))
        except:
            self.delete()
            return None

    def __unicode__(self):
        return u'%s,%s,%s' % (self.champ.name, self.flt, self.cls)


# noinspection PyUnresolvedReferences
class Rally(MyModel):
    name = db.StringProperty()
    short_name = db.StringProperty()
    import_name = db.StringProperty()
    champ = db.ReferenceProperty(Champ, required=True)
    start_date = db.DateProperty()
    end_date = db.DateProperty()
    leg_count = db.IntegerProperty(default=1)
    session_count = db.IntegerProperty(default=1)
    session_import_names = MyStringListProperty()
    result_type = db.StringProperty(required=True, choices=RESULT_TYPES, default='Best')
    stage_count = db.IntegerProperty(default=6)
    stage_names = db.StringListProperty(default=['SS' + str(n) for n in range(1, 7)])
    image_link = db.LinkProperty()
    image_data = db.BlobProperty()
    imported_roadbook = db.BooleanProperty()
    imported_results = db.ListProperty(int)

    @classmethod
    def rallies_in_progress(cls, count=0):
        today = time_util.today()
        # cannot use two inequality tests in one query, so filter end date manually
        rallies = cls.all().filter('start_date <=', today).order('start_date')
        rallies = itertools.ifilter(lambda r: today <= r.end_date, rallies)
        rallies = itertools.ifilter(cls.is_visible, rallies)
        if count > 0:
            rallies = itertools.islice(rallies, count)
        return list(rallies)

    @classmethod
    def rallies_in_future(cls, count=0):
        rallies = cls.all().filter('start_date >', time_util.today()).order('start_date')
        rallies = itertools.ifilter(cls.is_visible, rallies)
        if count > 0:
            rallies = itertools.islice(rallies, count)
        return list(rallies)

    @classmethod
    def rallies_in_progress_or_future(cls, count=0):
        rallies = cls.all().filter('end_date >=', time_util.today()).order('end_date')
        rallies = itertools.ifilter(cls.is_visible, rallies)
        if count > 0:
            rallies = itertools.islice(rallies, count)
        return list(rallies)

    @classmethod
    def rallies_in_past(cls, count=0):
        rallies = cls.all().filter('end_date <', time_util.today()).order('-end_date')
        rallies = itertools.ifilter(cls.is_visible_for_results, rallies)
        if count > 0:
            rallies = itertools.islice(rallies, count)
        return list(rallies)

    @classmethod
    def all_rallies_sortby_date(cls, since=None):
        rallies = cls.all().order('start_date').order('name')
        if since is not None:
            rallies = rallies.filter('start_date >', since)
        return filter(cls.is_visible, rallies)

    def get_plugin_name(self):
        return self.champ.get_plugin_name()

    def sdate_str(self):
        return time_util.format_date(self.start_date)

    def sdate_short_str(self):
        return time_util.format_date_short(self.start_date)

    def edate_str(self):
        return time_util.format_date(self.end_date)

    def edate_short_str(self):
        return time_util.format_date_short(self.end_date)

    def period_str(self):
        return '%s to %s' % (self.sdate_str(), self.edate_str())

    def period_short_str(self):
        return '%s - %s' % (self.sdate_short_str(), self.edate_short_str())

    def stage_nrs(self):
        return range(1, self.stage_count + 1)

    def delete(self):
        self.delete_results_cache()
        for stage in self.stage_set:
            stage.delete()
        for result in self.rawresult_set:
            result.delete()
        super(Rally, self).delete()

    def is_use_best_result(self):
        return self.result_type == 'Best'

    def is_use_first_result(self):
        return self.result_type == 'First'

    def is_use_last_result(self):
        return self.result_type == 'Last'

    def is_visible(self):
        return self.champ.is_non_hidden()

    def is_roadbook_available(self):
        days = self.champ.roadbook_publication_days
        return days <= 0 or time_util.occurred(self.start_date, days)

    def roadbook_publish_date(self):
        days = self.champ.roadbook_publication_days
        return time_util.today() if days <= 0 else time_util.date_offset(self.start_date, -days)

    def roadbook_publish_date_str(self):
        return time_util.format_date(self.roadbook_publish_date())

    def is_weather_forecast_available(self):
        days = self.champ.weather_forecast_days
        return days < 0 or time_util.occurred(self.start_date, days)

    def is_multisession(self):
        return self.session_count > 1

    def get_import_name(self, session=0):
        name = self.import_name or self.name
        if session and self.session_import_names:
            # noinspection PyTypeChecker
            count = len(self.session_import_names)
            if count > 1 and count >= session:
                name = self.session_import_names[session - 1]
            elif self.session_count > 1:
                name += str(session)
        return name

    def stages(self):
        return self.stage_set.order('nr')

    def get_stage(self, nr):
        if not nr:
            return None
        return self.stage_set.filter('nr', nr).get()

    def is_multiple_legs(self):
        return self.leg_count > 1

    def get_leg_for_stage(self, ss):
        if self.leg_count <= 1:
            return 1
        if not ss:
            return self.leg_count
        leg = 0
        for stage in self.stage_set.order('nr').fetch(ss):
            if stage.nr_in_leg == 1:
                leg += 1
        return leg or 1

    def get_stagenr_in_leg(self, ss):
        stage = self.get_stage(ss)
        if stage and stage.nr_in_leg:
            return stage.nr_in_leg
        return ss

    def generate_stages(self):
        stage_map = {}
        for stage in self.stage_set:
            stage_map[stage.nr] = stage
        stage_names = self.stage_names
        name_count = len(stage_names)
        for nr in range(1, self.stage_count + 1):
            default_name = 'SS' + str(nr)
            stage = stage_map.get(nr)
            if not stage:
                stage = Stage(nr=nr, rally=self)
                if nr <= name_count:
                    stage.name = stage_names[nr - 1]
                else:
                    stage.name = default_name
                    stage_names.append(stage.name)
                stage.tyre_change = nr == 1
                stage.auto_fields()
                stage.put()
            else:
                if nr <= name_count:
                    stage_name = stage_names[nr - 1]
                    if stage_name != default_name:
                        if stage.name != stage_name:
                            stage.name = stage_name
                            stage.put()
                    else:
                        stage_names[nr - 1] = stage.name
                del stage_map[nr]
        self.stage_names = stage_names
        self.put()
        for s in stage_map.values():
            s.delete()

    def get_roadbook(self):
        stages = map(Stage.get_roadbook_stage, self.stage_set.order('nr'))
        return Roadbook(self.name, self.start_date, self.end_date, None, stages)

    def import_roadbook(self, roadbook):
        if roadbook.name:
            if not self.name:
                self.name = roadbook.name
            if not self.import_name:
                self.import_name = roadbook.name
        if roadbook.sdate:
            self.start_date = roadbook.sdate
        elif not self.start_date:
            self.start_date = time_util.today()
        if roadbook.edate:
            self.end_date = roadbook.edate
        elif not self.end_date:
            self.end_date = self.start_date
        if roadbook.img_link and not self.image_data:
            self.image_link = roadbook.img_link
            self.import_image()
        if self.champ.is_public():
            self.leg_count = 1
        self.stage_count = len(roadbook.stages)
        self.imported_roadbook = self.stage_count > 0 and roadbook.stages[0].weather is not None
        self.put()
        if roadbook.stages:
            self.import_stages(roadbook.stages)

    def import_stages(self, stages):
        stage_map = {}
        stage_names = []
        for stage in self.stage_set:
            stage_map[stage.nr] = stage
        legs = 0
        for nr, rs in enumerate(stages):
            nr += 1
            stage = stage_map.get(nr)
            if not stage:
                stage = Stage(nr=nr, rally=self)
                stage_map[nr] = stage
            stage.import_roadbook_stage(rs)
            if rs.nr_in_leg == 1:
                legs += 1
            stage_names.append(stage.name)
        if stage_names:
            self.stage_names = stage_names
        if legs > self.leg_count:
            self.leg_count = legs
        self.put()

    def import_image(self, data=None):
        try:
            if not data and self.image_link:
                data = urllib2.urlopen(self.image_link).read()
            if data:
                image = images.Image(data)
                # keep aspect ratio
                width = RALLY_IMAGE_WIDTH
                height = image.height * width / image.width
                image.resize(width, height)
                self.image_data = db.Blob(image.execute_transforms(RALLY_IMAGE_TYPE))
                return self.image_data
        except Exception, e:
            logging.exception('Could not import image: %s', e)
        return None

    def is_finished(self):
        return time_util.today() > self.end_date

    def is_in_progress(self):
        return self.start_date <= time_util.today() <= self.end_date

    def is_future(self):
        return time_util.today() < self.start_date

    def are_results_imported(self, stage_nr=0):
        return stage_nr in self.imported_results

    def are_live_results_available(self):
        today = time_util.today()
        return self.start_date <= today and self.champ.is_public() or self.end_date < today

    def is_visible_for_results(self):
        return self.is_visible() and 0 in self.imported_results

    def show_provisional_results(self):
        return not self.are_results_imported() and self.champ.is_public() and self.are_live_results_available()

    def process_results(self, results, flt, cls=None):
        return self.champ.process_results(results, flt, cls, self.end_date)

    def get_results(self, stage_nr):
        #if not stage_nr:
        #    results = self.rawresult_set.filter('stage', stage_nr).order('status').order('time')
        #else:
        results = self.rawresult_set.filter('stage', stage_nr).order('time')
        return map(RawResult.get_result, list(results))

    def import_results(self, stage_nr, results):
        results = self.prepare_results_for_import(results)
        result_map = {}
        for obj in self.rawresult_set.filter('stage', stage_nr):
            result_map[obj.plugin_id] = obj
        for result in results:
            obj = result_map.get(result.plugin_id)
            if obj is None:
                obj = RawResult(rally=self, stage=stage_nr)
                obj.import_result(result)
        if not stage_nr in self.imported_results:
            self.imported_results.append(stage_nr)
            self.put()

    def reimport_overall_results(self, results):
        results = self.prepare_results_for_import(results)
        for obj in self.rawresult_set.filter('stage', 0):
            obj.delete()
        for result in results:
            obj = RawResult(rally=self, stage=0)
            obj.import_result(result)
        if 0 not in self.imported_results:
            self.imported_results.append(0)
            self.put()

    def prepare_results_for_import(self, results):
        return self.champ.prepare_results_for_import(results, self)

    def clear_overall_results(self):
        self.clear_stage_results(0)

    def clear_stage_results(self, stage_nr):
        for result in self.rawresult_set.filter('stage', stage_nr):
            result.delete()
        if stage_nr in self.imported_results:
            self.imported_results.remove(stage_nr)
            self.put()

    def clear_results(self):
        for result in self.rawresult_set:
            result.delete()
        self.imported_results = []
        self.put()

    def participants(self):
        return self.champ.participants(self.end_date)

    def delete_driver_results(self, plugin_ids):
        results = self.rawresult_set.filter('plugin_id IN', plugin_ids)
        for result in results:
            result.delete()

    def _get_results_cache(self, cls, flt):
        return self.resultscache_set.filter('cls', cls or '').filter('flt', flt).get()

    def get_overall_results(self, cls, flt):
        s = self._get_results_cache(cls, flt)
        return s and s.get_results() or None

    def set_overall_results(self, results, cls, flt):
        if results:
            r = self._get_results_cache(cls, flt) or ResultsCache(rally=self, cls=cls or '', flt=flt)
            r.set_results(results)
            r.put()

    def delete_results_cache(self):
        for r in self.resultscache_set:
            r.delete()

    def __unicode__(self):
        return self.name


# noinspection PyUnresolvedReferences
class Stage(MyModel):
    nr = db.IntegerProperty(required=True)
    nr_in_leg = db.IntegerProperty(default=0)
    name = db.StringProperty()
    rbr_name = db.StringProperty()
    rally = db.ReferenceProperty(Rally, required=True)
    mod = db.StringProperty()
    weather = db.StringProperty()
    wear = db.StringProperty()
    grip = db.StringProperty()
    tyre_change = db.BooleanProperty(default=False)
    time_of_day = db.StringProperty()

    @classmethod
    def get_rank_by_stage(cls, rbr_name, car=None, limit=25):
        stages = cls.all().filter('rbr_name', rbr_name)
        results = []
        for stage in stages:
            results += stage.results(car, limit)
        if len(results):
            results = map(RawResult.get_result2, results)
            results.sort()
            results = results[:limit]
            first = results[0]
            ps = PointsSystemMap['Empty']
            reduce(lambda prev, result: result.calc_points(first, prev, ps), results, first)
        return results

    def fixed_car(self):
        return self.rally.champ.fixed_car

    def auto_fields(self):
        if not self.name:
            self.name = (u'SS%s' % self.nr)
        if not self.mod:
            self.mod = u'Normal'
        if not self.weather:
            self.weather = u'Good'
        if not self.wear:
            self.wear = u'New'
        if not self.grip:
            self.grip = u'Dry'
        if not self.time_of_day:
            self.time_of_day = u'Day'
        if not self.nr_in_leg:
            self.nr_in_leg = self.nr
        if self.tyre_change is None:
            self.tyre_change = self.nr_in_leg == 1

    def import_roadbook_stage(self, rs):
        if rs.stage_name:  # and self.has_default_name():
            self.name = rs.stage_name
        if rs.rbr_stage_name:
            self.rbr_name = rs.rbr_stage_name
        if rs.mod:
            self.mod = rs.mod
        if rs.weather:
            self.weather = rs.weather
        if rs.wear:
            self.wear = rs.wear
        if rs.grip:
            self.grip = rs.grip
        if rs.nr_in_leg:
            self.nr_in_leg = rs.nr_in_leg
        if rs.tyre_change is not None:
            self.tyre_change = rs.tyre_change
        if rs.time_of_day:
            self.time_of_day = rs.time_of_day
        self.auto_fields()
        self.put()

    def get_roadbook_stage(self):
        return RoadbookStage(self.name, self.rbr_name, self.mod, self.weather, self.tyre_change, self.time_of_day,
                             self.nr_in_leg, self.wear, self.grip)

    def has_default_name(self):
        return not self.name or re.match('^\s*\w\w\s*\d+\s*$', self.name) is not None

    def is_first_in_leg(self):
        return self.nr == 1 or self.nr_in_leg == 1

    def are_results_imported(self):
        return self.rally.are_results_imported(self.nr)

    def clear_results(self):
        self.rally.clear_stage_results(self.nr)

    def results(self, car=None, limit=1000):
        q = self.rally.rawresult_set.filter('stage', self.nr)
        if car:
            q = q.filter('car', car)
        results = q.order('time').fetch(limit)

        def extend(result):
            result.mod = self.mod
            result.weather = self.weather
            result.wear = self.wear
            result.grip = self.grip
            return result

        return map(extend, results)

    def __unicode__(self):
        if self.rbr_name:
            return u'%s (%s)' % (self.name, self.rbr_name) if self.name else self.rbr_name
        return self.name or (u'SS%s' % self.nr)


# noinspection PyUnresolvedReferences
class RawResult(MyModel):
    rally = db.ReferenceProperty(Rally, required=True)
    stage = db.IntegerProperty(required=True)
    plugin_id = db.StringProperty()
    name = db.StringProperty()
    country = db.StringProperty()
    car_class = db.StringProperty()
    car = db.StringProperty()
    split1 = db.IntegerProperty()
    split2 = db.IntegerProperty()
    time = db.IntegerProperty()
    session = db.IntegerProperty()
    status = db.IntegerProperty()

    def get_result(self):
        return Result(self.plugin_id, self.name, self.country, self.car_class, self.car,
                      self.split1 or time_util.INVALID_TIME, self.split2 or time_util.INVALID_TIME,
                      self.time or time_util.INVALID_TIME,
                      self.session, self.status)

    def get_result2(self):
        result = self.get_result()
        result.rally = self.rally
        result.mod = self.mod
        result.weather = self.weather
        result.wear = self.wear
        result.grip = self.grip
        return result

    def import_result(self, result):
        self.plugin_id = result.plugin_id
        self.name = result.name
        self.country = result.country
        self.car_class = result.car_class
        self.car = result.car
        if result.t1 != time_util.INVALID_TIME:
            self.split1 = result.t1
        if result.t2 != time_util.INVALID_TIME:
            self.split2 = result.t2
        if result.t3 != time_util.INVALID_TIME:
            self.time = result.t3
        self.session = result.session
        self.status = result.status
        self.put()

    def is_finished(self):
        return self.status == STATE_FINISHED

    def is_retired(self):
        return self.status == STATE_RETIRED

    def is_skipped(self):
        return self.status == STATE_SKIPPED

    def is_in_progress(self):
        return self.status == STATE_IN_PROGRESS


class ResultsCache(MyModel):
    rally = db.ReferenceProperty(Rally, required=True)
    flt = db.IntegerProperty(required=True, default=0)
    cls = db.StringProperty(default='')
    results = db.BlobProperty()  # compressed pickled list of processed Results

    def set_results(self, results):
        self.results = to_blob(results)

    def get_results(self):
        try:
            return from_blob(self.results)
        except:
            self.delete()
            return None

    def __unicode__(self):
        return u'%s,%s,%s' % (self.rally.name, self.flt, self.cls)


class NewsItem(MyModel):
    date = db.DateProperty()
    title = db.StringProperty()
    body = db.StringProperty(multiline=True)
    link = db.LinkProperty()

    @classmethod
    def latest(cls, sdate, edate):
        return cls.all().filter('date >=', sdate).filter('date <=', edate)

    def make_entry(self):
        return {'date': self.date, 'link': self.link, 'title': self.title, 'body': self.body}


class Notification(MyModel):
    ntype = db.StringProperty(required=True, default='REPLAY_REMINDER')
    recipient = db.EmailProperty(required=True)
    rally_name = db.StringProperty(required=True)


def base_props(request):
    return {
        'user': users.get_current_user(),
        'admin': users.is_current_user_admin(),
        'result_manager': ResultManager.is_current_user_result_manager(),
        'login_url': users.create_login_url(request.uri),
        'logout_url': users.create_logout_url(request.uri),
        'now': time_util.now_str(),
        'year': settings.YEAR,
    }


def extended_props(request):
    p = base_props(request)
    p['filters'] = ALL_FILTERS
    p['champs'] = Champ.visible_champs()
    return p
