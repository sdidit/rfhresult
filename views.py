from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.api import users
from google.appengine.api import xmpp
from google.appengine.runtime import DeadlineExceededError
from context import Context
import settings
import models
#import forms
import context
import time_util
import logging
from urllib import unquote


def create_props(request, ctx=None, more_props=None):
    p = models.extended_props(request)
    if ctx:
        p.update(ctx.props())
    if more_props:
        p.update(more_props)
    css_url = request.get('css_url')
    if css_url:
        p['css_url'] = css_url
    return p


def send_xmpp_message(msg):
    user_address = settings.ADMIN_EMAIL
    try:
        if xmpp.get_presence(user_address):
            status_code = xmpp.send_message(user_address, msg)
            if status_code != xmpp.NO_ERROR:
                logging.error('XMPP failed with status ' + str(status_code))
                #else:
                #xmpp.send_invite(user_address)
    except:
        pass


def make_context(request, champ=None, rally=None):
    cls = request.get('class')
    if cls and champ and not champ.is_valid_result_class_name(cls):
        cls = None
    flt = request.get('filter')
    try:
        flt = int(flt) if flt else 0
    except ValueError:
        flt = 0
    if rally is not None:
        stage = request.get('stage')
        try:
            ss = int(stage) if stage else 0
        except ValueError:
            ss = 0
        if rally and (ss < 0 or ss > rally.stage_count):
            ss = 0
    else:
        ss = 0
    return Context(champ, rally, ss, cls, flt)


def write_xml(response, root_tag, title, entries_tag, entries, entry_tag, value_tags):
    response.headers['Content-Type'] = 'text/xml'
    out = response.out
    out.write('<?xml version="1.0" encoding="UTF-8" ?>\n')
    out.write('<' + root_tag)
    if title:
        out.write(' name="' + title + '"')
    out.write('>\n')
    if entries_tag:
        out.write('  <' + entries_tag + '>\n')
    for entry in entries:
        out.write('    <' + entry_tag + '>\n')
        for value_tag in value_tags:
            out.write('      <' + value_tag + '>' + entry[value_tag] + '</' + value_tag + '>\n')
        out.write('    </' + entry_tag + '>\n')
    if entries_tag:
        out.write('  </' + entries_tag + '>\n')
    out.write('</' + root_tag + '>\n')


def write_csv(response, comment, header, lines, name=None):
    response.headers['Content-Type'] = 'text/csv'
    if name:
        response.headers['Content-Disposition'] = 'attachment; filename=%s.csv' % str(name)
    sep = '\t'
    if comment:
        response.out.write(comment + '\r\n')
    if header:
        response.out.write(sep.join(header) + '\r\n')
    for line in lines:
        response.out.write(sep.join(line) + '\r\n')


class PageNotFoundHandler(webapp.RequestHandler):
    def get(self):
        self.error(404)
        p = create_props(self.request, None, {'title': 'Page Not Found'})
        p['host_url'] = self.request.host_url
        self.response.out.write(template.render(settings.template_path('404.html'), p))


class LogoutHandler(webapp.RequestHandler):
    def get(self):
        self.redirect(users.create_logout_url('/'))


class LoginHandler(webapp.RequestHandler):
    def get(self):
        self.redirect(users.create_login_url('/'))


class MainHandler(webapp.RequestHandler):
    def get(self):
        past_rallies = models.Rally.rallies_in_past(2)
        news = context.collect_news(settings.MAX_AGE_NEWS)
        for n in news:
            n['date_str'] = time_util.format_date(n['date'])
        for rally in past_rallies:
            if rally.are_results_imported():
                ctx = Context(rally.champ, rally, flt=1)
                results = ctx.cached_results()
                if results:
                    first = results[0]
                    rally.winner = first.member_name or first.name
        self.response.out.write(template.render(settings.template_path('main.html'), create_props(self.request, None, {
            'title': 'mijnrally.nl Rally Center',
            'news': news,
            'in_progress': models.Rally.rallies_in_progress(),
            'past': past_rallies,
            'future': models.Rally.rallies_in_future(3),
        })))


class ScheduleHandler(webapp.RequestHandler):
    def get(self):
        events = context.Event.collect_events()
        next_rally = models.Rally.rallies_in_progress_or_future(1)
        next_event = context.Event(next_rally[0].start_date, next_rally[0]) if next_rally else None
        self.response.out.write(
            template.render(settings.template_path('main_schedule.html'), create_props(self.request, None, {
                'title': 'Schedule',
                'events': events,
                'next_event': next_event,
            })))


class MijnRallyScheduleHandler(webapp.RequestHandler):
    def get(self):
        events = context.Event.collect_events()
        next_rally = models.Rally.rallies_in_progress_or_future(1)
        next_event = context.Event(next_rally[0].start_date, next_rally[0]) if next_rally else None
        self.response.out.write(
            template.render(settings.template_path('mijnrally_schedule.html'), create_props(self.request, None, {
                'title': 'Schedule',
                'events': events,
                'next_event': next_event,
            })))


class MijnRallyUpcomingRalliesHandler(webapp.RequestHandler):
    def get(self):
        num = self.request.get('num')
        try:
            num = int(num) if num else 0
        except ValueError:
            num = 0
        width = self.request.get('width')
        try:
            width = int(width) if width else 0
        except ValueError:
            width = 0
        rallies = models.Rally.rallies_in_progress_or_future(num or 1)
        next_event = context.Event(rallies[0].start_date, rallies[0]) if rallies else None
        path = settings.template_path('mijnrally_upcoming_rallies.html')
        self.response.out.write(
            template.render(path, create_props(self.request, None, {
                'title': 'Toekomstige rallies',
                'rallies': rallies,
                'width': width,
                'next_event': next_event
            })))


class MemberListHandler(webapp.RequestHandler):
    def get(self):
        ctx = make_context(self.request)
        self.response.out.write(
            template.render(settings.template_path('main_member_list.html'), create_props(self.request, ctx, {
                'title': 'Members',
                'members': models.Member.all().filter('retired', False).order('name'),
            })))


class MemberHandler(webapp.RequestHandler):
    def get(self, member_id):
        ctx = make_context(self.request)
        member = models.Member.get(member_id)
        drivers = member.active_as()
        drivers.sort(key=lambda driver: driver.champ.name.lower())
        for driver in drivers:
            driver.pos = ctx.driver_pos_str(driver)
        self.response.out.write(
            template.render(settings.template_path('main_member.html'), create_props(self.request, ctx, {
                'title': unicode(member),
                'member': member,
                'drivers': drivers,
            })))


class TeamListHandler(webapp.RequestHandler):
    def get(self):
        ctx = make_context(self.request)
        self.response.out.write(
            template.render(settings.template_path('main_team_list.html'), create_props(self.request, ctx, {
                'title': 'Teams',
                'teams': models.Team.all().order('name'),
            })))


class TeamBannerHandler(webapp.RequestHandler):
    def get(self, obj_id):
        team = models.Team.get(obj_id)
        if team.banner:
            self.response.headers['Content-Type'] = models.TEAM_IMAGE_CONTENT_TYPE
            self.response.headers['max-age'] = str(3 * time_util.SECS_IN_DAY)
            self.response.out.write(team.banner)


class StageRankHandler(webapp.RequestHandler):
    def get(self, stage_id):
        try:
            car = None
            try:
                stage = models.Stage.get(stage_id)
                rbr_name = stage.rbr_name
                car = stage.fixed_car()
            except:
                rbr_name = unquote(stage_id)
            rank = models.Stage.get_rank_by_stage(rbr_name, car)
            ctx = make_context(self.request)
            title = 'Fastest stage times for ' + rbr_name
            if car:
                title += ' with ' + car
            self.response.out.write(template.render(
                settings.template_path('main_stage_rank.html'), create_props(self.request, ctx, {
                    'title': title,
                    'results': rank,
                })))
        except DeadlineExceededError:
            self.response.clear()
            self.response.set_status(500, "This operation could not be completed in time...")


class TeamCupStandingsHandler(webapp.RequestHandler):
    def get(self):
        ctx = make_context(self.request)
        standings = ctx.team_standings_with_charts()
        self.response.out.write(
            template.render(settings.template_path('main_teamcup_standings.html'), create_props(self.request, ctx, {
                'title': 'Team Cups',
                'standings': standings,
            })))


class MijnRallyTeamCupTopStandingsHandler(webapp.RequestHandler):
    def get(self):
        try:
            ctx = make_context(self.request)
            num = self.request.get('num')
            try:
                num = int(num) if num else 0
            except ValueError:
                num = 0
            standings = ctx.top_n_teamcup_standings(num)
            self.response.out.write(template.render(
                settings.template_path('mijnrally_teamcup_top_standings.html'), create_props(self.request, ctx, {
                    'title': 'Team Cup Stand',
                    'standings': standings,
                    'top': num,
                })))
        except DeadlineExceededError:
            self.response.clear()
            self.response.set_status(500, "This operation could not be completed in time...")


class ChampListHandler(webapp.RequestHandler):
    def get(self):
        ctx = make_context(self.request)
        self.response.out.write(
            template.render(settings.template_path('main_champ_list.html'), create_props(self.request, ctx, {
                'title': 'Championships',
                'champs': models.Champ.non_hidden_champs(),
            })))


class ChampStandingsHandler(webapp.RequestHandler):
    def get(self, obj_id):
        try:
            champ = models.Champ.get(obj_id)
            ctx = make_context(self.request, champ)
            self.response.out.write(template.render(
                settings.template_path('main_champ_standings.html'), create_props(self.request, ctx, {
                    'title': unicode(champ),
                    'rallies': champ.rallies(),
                    'standings': ctx.cached_standings(),
                    'top3': champ.closed and ctx.top_three_standings(),
                    'chart_url': ctx.standings_line_chart(),
                })))
        except DeadlineExceededError:
            self.response.clear()
            self.response.set_status(500, "This operation could not be completed in time...")


class MijnRallyChampTopStandingsHandler(webapp.RequestHandler):
    def get(self, champ_id):
        try:
            champ = models.Champ.get(champ_id)
            ctx = make_context(self.request, champ)
            ctx.flt = 1
            ctx.cls = None
            num = self.request.get('num')
            try:
                num = int(num) if num else 0
            except ValueError:
                num = 0
            standings = ctx.top_n_standings(num)
            # until first race, show standings of last year
            if not standings and champ_id == 'agpzfnJmaC0yMDE0chILEgVDaGFtcBiAgICAgOTRCgw':
                champ_id = 'agpzfnJmaC0yMDEzcgwLEgVDaGFtcBjRDww'
                self.redirect("http://rfh-2013.appspot.com/mijnrally/champ/%s/top/?filter=1&num=%d" % (champ_id, num))
            else:
                self.response.out.write(template.render(
                    settings.template_path('mijnrally_champ_top_standings.html'), create_props(self.request, ctx, {
                        'title': unicode(champ),
                        'standings': standings,
                        'top': num,
                    })))
        except DeadlineExceededError:
            self.response.clear()
            self.response.set_status(500, "This operation could not be completed in time...")


class ChampDriversHandler(webapp.RequestHandler):
    def get(self, obj_id):
        champ = models.Champ.get(obj_id)
        ctx = make_context(self.request, champ)
        drivers = champ.participants(cls=ctx.cls)
        self.response.out.write(template.render(
            settings.template_path('main_champ_drivers.html'), create_props(self.request, ctx, {
                'title': unicode(champ),
                'filters': (models.FILTER_RFH,),  # override with only RFH filter
                'rallies': champ.rallies(),
                'drivers': drivers,
            })))


class ChampResultsHandler(webapp.RequestHandler):
    def get(self, obj_id):
        champ = models.Champ.get(obj_id)
        ctx = make_context(self.request, champ)
        self.response.out.write(template.render(
            settings.template_path('main_champ_results.html'), create_props(self.request, ctx, {
                'title': unicode(champ),
                'rallies': champ.rallies(),
            })))


class RallyHandler(webapp.RequestHandler):
    def get(self, rally_id):
        rally = models.Rally.get(rally_id)
        champ = rally.champ
        ctx = make_context(self.request, champ, rally)
        self.response.out.write(template.render(
            settings.template_path('main_rally_roadbook.html'), create_props(self.request, ctx, {
                'title': unicode(champ),
            })))


class CSVRallyHandler(webapp.RequestHandler):
    def get(self, rally_id, name):
        rally = models.Rally.get(rally_id)
        roadbook = context.csv_roadbook(rally)
        if roadbook:
            comment, header, stages = roadbook
            write_csv(self.response, comment, header, stages, name)
        else:
            self.redirect('/rally/%s/' % str(rally.key()))


class RallyResultsHandler(webapp.RequestHandler):
    def get(self, rally_id):
        try:
            rally = models.Rally.get(rally_id)
            champ = rally.champ
            ctx = make_context(self.request, champ, rally)
            self.response.out.write(template.render(
                settings.template_path('main_rally_results.html'), create_props(self.request, ctx, {
                    'title': unicode(champ),
                    'results': ctx.cached_results(),
                    'team_results': ctx.team_results(),
                    'provisional': False,
                    'full_results_url': ctx.full_results_url(),
                })))
        except DeadlineExceededError:
            self.response.clear()
            self.response.set_status(500, "This operation could not be completed in time...")


class MijnRallyRallyTopResultsHandler(webapp.RequestHandler):
    def get(self, rally_id=None):
        try:
            num = self.request.get('num')
            try:
                num = int(num) if num else 0
            except ValueError:
                num = 0
            if rally_id is not None:
                rally = models.Rally.get(rally_id)
            else:
                rallies = models.Rally.rallies_in_past(1)
                rally = rallies[0] if rallies else None
                if not rally:
                    # before first rally show last rally of last year
                    self.redirect('http://rfh-2013.appspot.com/mijnrally/rally/results/top/?num=%d' % num)
                    return
            if rally:
                champ = rally.champ
                title = unicode(champ)
                ctx = make_context(self.request, champ, rally)
                ctx.flt = 1
                ctx.cls = None
                results = ctx.cached_results()
                if num and num < len(results):
                    results = results[:num]
            else:
                title = ''
                ctx = make_context(self.request)
                results = None
            self.response.out.write(template.render(
                settings.template_path('mijnrally_rally_top_results.html'), create_props(self.request, ctx, {
                    'title': title,
                    'results': results,
                })))
        except DeadlineExceededError:
            self.response.clear()
            self.response.set_status(500, "This operation could not be completed in time...")


class RallyLiveResultsHandler(webapp.RequestHandler):
    def get(self, rally_id):
        rally = models.Rally.get(rally_id)
        if rally.are_live_results_available():
            try:
                champ = rally.champ
                ctx = make_context(self.request, champ, rally)
                self.response.out.write(template.render(
                    settings.template_path('main_rally_results.html'), create_props(self.request, ctx, {
                        'title': unicode(champ),
                        'results': ctx.live_results(),
                        'provisional': True,
                    })))
            except DeadlineExceededError:
                self.response.clear()
                self.response.set_status(500, "This operation could not be completed in time...")
        else:
            self.error(404)


class RallyImageHandler(webapp.RequestHandler):
    def get(self, obj_id):
        rally = models.Rally.get(obj_id)
        if rally.image_data:
            self.response.headers['Content-Type'] = models.RALLY_IMAGE_CONTENT_TYPE
            self.response.headers['max-age'] = str(3 * time_util.SECS_IN_DAY)
            self.response.out.write(rally.image_data)


class CSVRallyResultsHandler(webapp.RequestHandler):
    # noinspection PyUnusedLocal
    def get(self, rally_id, name):
        rally = models.Rally.get(rally_id)
        champ = rally.champ
        ctx = make_context(self.request, champ, rally)
        results = ctx.csv_results()
        write_csv(self.response, None, None, results)


class XMLChampListHandler(webapp.RequestHandler):
    def get(self):
        def xml(champ):
            return {'id': str(champ.key()), 'name': unicode(champ)}

        champs = [xml(champ) for champ in models.Champ.non_hidden_champs()]
        tags = ('id', 'name')
        write_xml(self.response, 'champs', None, None, champs, 'champ', tags)


class XMLChampStandingsHandler(webapp.RequestHandler):
    # noinspection PyUnusedLocal
    def get(self, champ_id, name):
        champ = models.Champ.get(champ_id)
        ctx = make_context(self.request, champ)
        num = self.request.get('num')
        try:
            num = int(num) if num else 0
        except ValueError:
            num = 0
        standings = ctx.top_n_standings(num)
        for s in standings:
            if not s['member_name']:
                s['member_name'] = s['name']
        tags = ('pos', 'name', 'member_name', 'team', 'country', 'points')
        write_xml(self.response, 'champ', unicode(champ), 'standings', standings, 'entry', tags)


class XMLChampResultsHandler(webapp.RequestHandler):
    def get(self, champ_id):
        def xml(rally):
            return {'id': str(rally.key()), 'name': unicode(rally), 'start': str(rally.start_date),
                    'end': str(rally.end_date)}

        champ = models.Champ.get(champ_id)
        rallies = map(xml, champ.rallies())
        tags = ('id', 'name', 'start', 'end')
        write_xml(self.response, 'champ', unicode(champ), 'rallies', rallies, 'rally', tags)


class ToolsHandler(webapp.RequestHandler):
    def render_form(self, name='', error=None):
        self.response.out.write(template.render(
            settings.template_path('main_tools.html'), create_props(self.request, None, {
                'title': 'Tools',
                'error': error,
                'name': name,
                'roadbook_names': context.roadbook_names(),
            })))

    def get(self):
        self.render_form()


class ToolsRoadbookCSVHandler(ToolsHandler):
    def post(self):
        name1 = self.request.POST.get('name')
        name2 = self.request.POST.get('name2')
        name = name1 or name2
        # use .csv to make it a download because rendering of .txt is very browser specific
        if name:
            name = name.strip()
            roadbook = context.csv_roadbook(name)
            if roadbook:
                comment, header, stages = roadbook
                write_csv(self.response, comment, header, stages, name)
            else:
                self.render_form(name=name, error='Roadbook not found')
        else:
            self.render_form(error='Please enter a rally name')
