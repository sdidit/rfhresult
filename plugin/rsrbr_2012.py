import urllib
import urllib2
import logging
import re
import datetime
import time_util
from google.appengine.api import urlfetch
from google.appengine.api import memcache
from models import Result, Roadbook, RoadbookStage, STATE_FINISHED, STATE_RETIRED, STATE_SKIPPED, STATE_IN_PROGRESS


TIMEOUT_SECS = 60

domain = 'xii.rallyesim.com'

url_base = 'http://' + domain + '/'

news_url                 = url_base + 'mod_view.asp' # pass id for specific news item, no id for latest, id=0 for empty (news selector combobox only)

rally_express_url        = url_base + 'results_express.asp'
rally_official_url       = url_base + 'results_rank_rally.asp'    #http://xii.rallyesim.com/results_rank_rally.asp?rallye=Tank_S_Rally&order=temps%20ASC
rally_official_stage_url = url_base + 'classement_es.asp' #http://x.rallyesim.com/classement_es.asp?rallye=Tank_S_Rally&stage=1&order=
rally_live_url           = url_base + 'classement_live.asp'
rally_live_stage_url     = url_base + 'live.asp'
rally_live_index_url     = url_base + 'index_live.asp'
roadbook_list_url        = url_base + 'roadbook_view.asp'
roadbook_url             = url_base + 'mod_select_roadbook.asp'
roadbook2_url            = url_base + 'roadbook.asp'
public_roadbook_url      = url_base + 'public_index_live.asp'
public_rally_url         = url_base + 'public_classement_live.asp'
public_stage_url         = url_base + 'public_live.asp'

# --- public session ----------------------------------------------------------------------#
#       roadbook: public_roadbook_url + '?rallye=RFH%20EVO%206%20TME%20Cup%20Gravel5'      #
# overall result: public_rally_url    + '?rallye=RFH%20EVO%206%20TME%20Cup%20Gravel5'      #
#   stage result: public_stage_url    + '?rallye=RFH%20EVO%206%20TME%20Cup%20Gravel5&ES=1' #
# -----------------------------------------------------------------------------------------#

# temporarily get express rally results from here, else we get a timeout
tmp_rally_url = None #'http://www.xs4all.nl/~rsdi/rfh/2012/'




# mmm:ss.hh (hours are not included)
time_regex = re.compile('(\d+):(\d\d)\.(\d\d)')

wrc2012_cars = {'Mini WRC', 'VW Polo WRC Concept'}

# all packs not in this dict will have a class equal to first 2 characters of pack
pack_to_class = {
    'A8W'      : 'A8W',
    'RR1'      : 'R1', # does not exist?
    'RR2'      : 'R2',
    'RR3'      : 'R3',
    'S2000'    : 'S2000',
    'S20004S'  : 'S2000',
    'N4S'      : 'S2000',
    'VHC0'     : 'VHC',
    'F200012'  : 'F2000_12',
    'F200013'  : 'F2000_13',
    'F200014'  : 'F2000_14',
    'B'        : 'Group_B',
    'B0'       : 'Group_B',
    'Group_B'  : 'Group_B',
    'Group_H'  : 'Group_H',
    }

country_to_code = {
    'pay': 'nl',
    'hog': 'hu',
    'all': 'de',
    'sui': 'ch',
    'dan': 'dk',
    'por': 'pt',
    'cri': 'hr',
    'thq': 'cz',
    'mad': 'mg',
    'and': 'ad',
    'mex': 'mx',
    'pol': 'pl',
    'tur': 'tr',
    'maq': 'mq',
    'slv': 'si',
    'eta': 'us',
    'gra': 'uk',
    'sue': 'se',
    'let': 'lv',
    'bul': 'bg',
    'cat': 'ct',
    'par': 'py',
    'isr': 'il',
    'bar': 'bb',
    'sri': 'lk',
    'jam': 'jm',
    'equ': 'ec',
    'lit': 'lt',
    'csr': 'cr',
    }

def fetch(url, data = None):
    if data:
        url += '?' + data
    return urlfetch.fetch(url, deadline = TIMEOUT_SECS)

def _decode(s):
    return s.decode('latin-1')


def parse_car_class(pack, car):
    if pack == 'A8W' and car:
        if car.endswith("2011"):
            return pack + '-11'
        if car in wrc2012_cars or car.endswith('2012'):
            return pack + '-12'
    return pack_to_class.get(pack) or _decode(pack[:2])

def parse_official_car_class(pack, car):
    if pack == 'A8W-11':
        return pack
    if pack == 'A8' and car:
        if car in wrc2012_cars or car.endswith('2012'):
            return 'A8W-12'
        if 'WRC' in car:
            return 'A8W'
    return pack_to_class.get(pack) or _decode(pack[:2])

def parse_country(country):
    return country_to_code.get(country) or _decode(country[:2])

def parse_time(s):
    m = time_regex.match(s)
    if not m:
        return time_util.INVALID_TIME
    return (int(m.group(1)) * 60 + int(m.group(2))) * 100 + int(m.group(3))

status_dict = { 'finished': STATE_FINISHED, 'retired': STATE_RETIRED, 'skipped': STATE_SKIPPED, 'in progress': STATE_IN_PROGRESS }

def parse_status(status):
    return status_dict.get(status.strip().lower()) or STATE_FINISHED

def parse_official_status(status):
    return STATE_FINISHED if not status or status == '-' else STATE_RETIRED

def official_key(name):
    return 'official_' + name

def official_results(name, ss = 0):
    key = official_key(name)
    official = memcache.get(key)
    if official is not None and not official:
        return
    n = name.replace(' ', '_')
    if ss == 0:
        def convert_rally(result):
            plugin_id, country, name, car, pack, time, status = result
            car = _decode(car)
            return Result(_decode(plugin_id), _decode(name), parse_country(country), parse_official_car_class(pack, car), car,
                          time_util.INVALID_TIME, time_util.INVALID_TIME, parse_time(time),
                          0, parse_official_status(status))

        convert = convert_rally
        url = '%s?rallye=%s&where=&order=%s' % (rally_official_url, n, urllib.quote('temps ASC'))
        # plugin_id, country/name, car, pack, time
        regex = re.compile(
            '<tr .*>\s*' +
            '<td .*</td>\s*' +
            '<td .*<b>(\d+)</b>&nbsp;</font></td>\s*' +
            '<td .*<b><img src=.*/(\w+)\.jpg .*> (.+)</b></font><br><font color="#000000" size="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.+)</font></td>\s*' +
            '<td .*>\s*.*\s*</font></td>\s*' +
            '<td .*>\s*.*\s*</font></td>\s*' +
            '<td .* size="1"><b>(.+)</b>\s*.*\\s*</font></td>\s*' +
            '<td .*class="blackredlink">([0-9:\.]+)</a>.*</td>\s*' +
            '<td .*&nbsp;\s*.*</td>\s*' +
            '<td .*&nbsp;\s*.*</td>\s*' +
            '<td .*</td>\s*' +
            '<td .*</td>\s*' +
            '<td .*</td>\s*' +
            '<td .* size="1">(.+)<.*</td>')
    else:
        # plugin_id, country/name, mod/pack, car, split1, split2, time
        def convert_stage(result):
            plugin_id, country, name, mod, pack, car, split1, split2, time = result
            status = 'finished' # todo
            car = _decode(car)
            return Result(_decode(plugin_id), _decode(name), parse_country(country), parse_car_class(pack, car), car,
                          parse_time(split1), parse_time(split2), parse_time(time),
                          0, parse_status(status))

        convert = convert_stage
        stage = str(ss)
        url = '%s?rallye=%s&stage=%s&order=' % (rally_official_stage_url, n, stage)
        # plugin_id, country/name, mod/pack, car, split1, split2, time
        regex = re.compile(
            '<tr>\s*' +
            '<td align="center" nowrap class="filets"><font size="1"><b>\d+</b></font></td>\s*' +
            '<td align="right" nowrap class="filets"><font size="1"><b>(\d+)</b></font></td>\s*' +
            '<td align="left" nowrap class="filets"><font size="1"><b><img src=.*/(\w+)\.jpg .*> (.+)</b></font></td>\s*' +
            '<td align="center" nowrap class="filets"><font size="1">(.+) - (.+)&nbsp;\s*</font></td>\s*' +
            '<td align="center" nowrap class="filets"><font size="1">(.+)&nbsp;\s*</font></td>\s*' +
            '<td align="center" nowrap class="filets">\s*<font size="1">([\d:\.]+)&nbsp;\s*</font></td>\s*' +
            '<td align="center" nowrap class="filets">\s*<font size="1">([\d:\.]+)&nbsp;\s*</font></td>\s*' +
            '<td align="center" nowrap class="filets"><font size="1"><b>([\d:\.]+)\s*&nbsp;</b></font></td>')
    try:
        result = fetch(url)
        if result.status_code != 200:
            logging.error('Could not get results: http error ' + str(result.status_code))
            return ()
        content = result.content
#        content = urllib2.urlopen(url).read()
        results = map(convert, regex.findall(content))
        memcache.add(key, len(results) > 0, 300)
        return results
    except:
        logging.exception('Could not get results')
        return ()

def full_results_url(name, ss = 0):
    return express_url(name, ss)

def express_url(name, ss = 0):
    stage = str(ss) if ss else 'All'
    params = { 'cmbNumero': name, 'cmbNumber': stage, 'cmbAfter': 'All', 'Submit': 'Send' }
    return rally_express_url + '?' + urllib.urlencode(params)

    
def express_results(name, ss = 0):
    if ss == 0:
        def convert_rally(result):
            plugin_id, country, name, car, group, cls, time, status = result
            pack = group + cls
            car = _decode(car)
            return Result(_decode(plugin_id), _decode(name), parse_country(country), parse_car_class(pack, car), car,
                          time_util.INVALID_TIME, time_util.INVALID_TIME, parse_time(time),
                          0, parse_official_status(status))

        convert = convert_rally
        # plugin_id, country/name, car, group, class, time
        regex = re.compile(
            '<tr .*>\s*' +
            '<td .*>.*</td>\s*' +
            '<td .*><b>(\d+)</b>.*</td>\s*' +
            '<td .*><img src=.*/(\w+)\.jpg .*> (.+)</b>.*</td>\s*' +
            '<td .*>(.+)&nbsp;.*</td>\s*' +
            '<td .*>(.+)&nbsp;.*</td>\s*' +
            '<td .*>(.+)&nbsp;.*</td>\s*' +
            '<td .*><b>\s*(.+)\s*&nbsp;\s*</b>.*</td>\s*' +
            '<td .*>\s*.*</td>\s*' +
            '<td .*>\s*.*</td>\s*' +
            '<td .*>\s*(.*)</font></td>')
    else:
        def convert_stage(result):
            plugin_id, country, name, car, group, cls, split1, split2, time, status = result
            pack = group + cls
            car = _decode(car)
            return Result(_decode(plugin_id), _decode(name), parse_country(country), parse_car_class(pack, car), car,
                          parse_time(split1), parse_time(split2), parse_time(time),
                          0, parse_status(status))

        convert = convert_stage
        # plugin_id, country/name, car, group, class, split1, split2, time, diff first, diff prev, status
        regex = re.compile(
            '<tr .*>\s*' +
            '<td .*>.*</td>\s*' +
            '<td .*><b>(\d+)</b>.*</td>\s*' +
            '<td .*><img src=.*/(\w+)\.jpg .*> (.+)</b>.*</td>\s*' +
            '<td .*>(.+)&nbsp;.*</td>\s*' +
            '<td .*>(.+)&nbsp;.*</td>\s*' +
            '<td .*>(.+)&nbsp;.*</td>\s*' +
            '<td .*>\s*(\S*)&nbsp;\s*.*</td>\s*' +
            '<td .*>\s*(\S*)&nbsp;\s*.*</td>\s*' +
            '<td .*>\s*(\S*)&nbsp;\s*.*</td>\s*' +
            '<td .*>\s*.*</td>\s*' +
            '<td .*>\s*.*</td>\s*' +
            '<td .*>\s*([\w ]+)</font></td>')
    
    try:
        content = ()
        if ss == 0 and tmp_rally_url:
            # use different url for now
            try:
                suffix = (' SS' + str(ss)) if ss else ''
                content = urllib2.urlopen(tmp_rally_url + urllib.quote(name + suffix) + '.html').read()
            except Exception, e:
                logging.exception('Could not get results from tmp url: ' + str(e))
        if not content:
            result = fetch(express_url(name, ss))
            if result.status_code != 200:
                logging.error('Could not get results: http error ' + str(result.status_code))
                return ()
            content = result.content
        results = regex.findall(content)
        logging.info('Results length = ' + str(len(results))) 
        return map(convert, results)
    except Exception, e:
        logging.exception('Could not get results: ' + str(e))
        return ()

def single_public_rally_result(rally, session):
    car = rally.champ.fixed_car
    pack = car and rally.champ.car_classes[0]

    def convert(result):
        plugin_id, country, name, time = result
        return Result(_decode(plugin_id), _decode(name), parse_country(country), pack, car,
                      time_util.INVALID_TIME, time_util.INVALID_TIME, parse_time(time),
                      session)

    # plugin_id, country/name, time, diff_leader, diff_prev
    regex = re.compile(
        '<tr .*>\s*' +
        '<td .*>.+ size="1">\d+&nbsp;&nbsp;</font></b></td>\s*' +
        '<td .*>.+ size="1"><b>(\d+)</b></font></td>\s*' +
        '<td .*>.+ size="1"><b><img src=.*/(\w+)\.jpg .*> (.+)&nbsp;</b></font></td>\s*' +
        '<td .*>.+ size="1">([0-9:\.]+)</font></td>\s*')
    rally_name = rally.get_import_name(session)
    url = '%s?rallye=%s' % (public_rally_url, urllib.quote(rally_name))

    try:
        content = urllib2.urlopen(url).read()
        return map(convert, regex.findall(content))

    except:
        logging.exception('Could not get results')
        return ()

def single_public_stage_result(rally, session, ss):
    def convert(result):
        plugin_id, country, name, mod, pack, car, split1, split2, time, status = result
        car = _decode(car)
        return Result(_decode(plugin_id), _decode(name), parse_country(country), parse_car_class(pack, car), car,
                      parse_time(split1), parse_time(split2), parse_time(time),
                      session, parse_status(status))

    # plugin_id, country/name, mod/pack, car, split1, split2, time, diff leader, diff prev, status
    regex = re.compile(
        '<tr .*>\s*' +
        '<td .*>.+</td>\s*' +
        '<td .*>.+<b>(\d+)</b>.+</td>\s*' +
        '<td .*>.+<b><img src=.*/(\w+)\.jpg .*> (.+)</b>.+</td>\s*' +
        '<td .*>.+ size="1">(.*) ?- ?(.*)&nbsp;\s*</font></td>\s*' +
        '<td .*>.+ size="1">(.+)&nbsp;\s*</font></td>\s*' +
        '<td .*>.+ size="1">\s*([\d:\.]*)&nbsp;</font></td>\s*' +
        '<td .*>.+ size="1">\s*([\d:\.]*)&nbsp;</font></td>\s*' +
        '<td .*>.+ size="1"><b>\s*([\d:\.]+)\s*&nbsp;</b></font></td>\s*' +
        '<td .*>.+ size="1">\s*[\d:\.\+\-]+\s*</font></td>\s*' +
        '<td .*>.+ size="1">\s*[\d:\.\+\-]+\s*</font></td>\s*' +
        '<td .*>.+ size="1">\s*([\w ]+)\s*</font></td>')
    rally_name = rally.get_import_name(session)
    url = '%s?rallye=%s&ES=%s' % (public_stage_url, urllib.quote(rally_name), ss)

    try:
        content = urllib2.urlopen(url).read()
        results = regex.findall(content)
        return map(convert, results)

    except:
        logging.exception('Could not get results')
        return ()

def single_public_result(rally, session, ss):
    if ss == 0:
        return single_public_rally_result(rally, session)
    else:
        return single_public_stage_result(rally, session, ss)

def merge_first_result(final_result, result):
    for r in result:
        pid = r.plugin_id
        # first session done by driver is the one that counts, no retries
        if pid not in final_result:
            final_result[pid] = r

def merge_last_result(final_result, result):
    for r in result:
        pid = r.plugin_id
        # last session done by driver is the one that counts
        final_result[pid] = r

def merge_best_result(final_result, result):
    for r in result:
        pid = r.plugin_id
        # best session done by driver is the one that counts
        old_result = final_result.get(pid)
        if old_result is None or old_result.t3 > r.t3:
            final_result[pid] = r

def public_results(rally, ss = 0):
    if rally.session_count <= 1:
        return single_public_result(rally, 0, ss)
    final_result = {}
    if rally.is_use_first_result():
        merge = merge_first_result
    elif rally.is_use_last_result():
        merge = merge_last_result
    else:
        merge = merge_best_result
    for session in range(1, rally.session_count + 1):
        result = single_public_result(rally, session, ss)
        merge(final_result, result)

    # public overall result lacks pack and car (unless there's a fixed car), get it from first stage result
    if ss == 0 and not rally.champ.fixed_car:
        session_ss1_results = {} # cache
        for pid, r in final_result.iteritems():
            if not r.car:
                ss1_result = session_ss1_results.get(r.session)
                if ss1_result is None:
                    raw_ss1_result = single_public_stage_result(rally, r.session, 1)
                    ss1_result = {}
                    for ss1_r in raw_ss1_result:
                        ss1_result[ss1_r.plugin_id] = ss1_r
                    session_ss1_results[r.session] = ss1_result
                ss1_driver_result = ss1_result.get(pid)
                if ss1_driver_result:
                    r.car = ss1_driver_result.car
                    r.car_class = ss1_driver_result.car_class

    return sorted(final_result.values())


def results(rally, ss = 0):
    if rally.champ.is_official():
        return official_results(rally.get_import_name(), ss)
    if rally.champ.is_unofficial():
        return express_results(rally.get_import_name(), ss)
    if rally.champ.is_public():
        return public_results(rally, ss)
    return ()


def live_results(rally, ss = 0):
    if rally.champ.is_public():
        return public_results(rally, ss)

    name = rally.get_import_name()
    if rally.leg_count > 1:
        leg_name = '%s Leg %s' % (name, rally.get_leg_for_stage(ss))
    else:
        leg_name = name
    ss_count = rally.stage_count
    params = '?rallye=%s&numero=0&view=x&LiveES=%s&c=%s' % (urllib.quote(leg_name), ss_count, ss_count)
    #'http://rscenter.rallyesim.com/rsrbrlive09/live.asp?pseudo=sdi&numero=1972&rallye=Paradigit_ELE_Rally_Leg_2&ES=9&pays=3&voiture=Subaru Impreza WRC 2007&PM=Tarmac Mod - A8W-07&view=sdi'

    if ss == 0:
        # plugin_id, country/name, car, group, class, time
        def convert_rally(result):
            plugin_id, country, name, mod, pack, car, time = result
            car = _decode(car)
            return Result(_decode(plugin_id), _decode(name), parse_country(country), parse_car_class(pack, car), car,
                          time_util.INVALID_TIME, time_util.INVALID_TIME, parse_time(time))

        convert = convert_rally
        url = rally_live_url + params

        # plugin_id, country/name, mod/pack, car, time
        regex = re.compile(
            '<tr .*>\s*' +
            '<td align="right" nowrap class="boutonclass"><font size="1"><b>\d+</b></font></td>\s*' +
            '<td align="right" nowrap="nowrap" class="boutonclass"><font size="1"><b>\s*.*\s*</b></font></td>\s*' +
            '<td align="right" nowrap class="filets"><font color="#[\dA-F]+" size="1"><b>(\d+)</b></font></td>\s*' +
            '<td align="left" nowrap class="filets"><font color="#[\dA-F]+" size="1"><b><img src=.*/(\w+)\.jpg .*> (.+)</b></font></td>\s*' +
            '<td align="center" nowrap class="filets"><font color="#[\dA-F]+" size="1">(.+) - (.+)&nbsp;</font></td>\s*' +
            '<td align="center" nowrap class="filets"><font color="#[\dA-F]+" size="1">(.+)&nbsp;</font></td>\s*' +
            '<td align="center" nowrap class="filets"><font color="#[\dA-F]+" size="1"><b>([0-9:\.]+)</b></font></td>\s*')
    else:
        # plugin_id, country/name, mod/pack, car, split1, split2, time
        def convert_stage(result):
            plugin_id, country, name, mod, pack, car, split1, split2, time = result
            status = 'finished' # todo
            car = _decode(car)
            return Result(_decode(plugin_id), _decode(name), parse_country(country), parse_car_class(pack, car), car,
                          parse_time(split1), parse_time(split2), parse_time(time),
                          0, parse_status(status))
        convert = convert_stage
        params += '&ES=%s' % rally.get_stagenr_in_leg(ss)
        url = rally_live_stage_url + params

        # plugin_id, country/name, mod/pack, car, split1, split2, time
        regex = re.compile(
            '<tr .*>\s*' +
            '<td align="right" nowrap class="boutonclass"><font size="1"><b>\d+&nbsp;&nbsp;</b></font></td>\s*' +
            '<td align="right" nowrap class="filets"><font color="#[\dA-F]+" size="1"><b>(\d+)</b></font></td>\s*' +
            '<td align="left" nowrap class="filets"><font color="#[\dA-F]+" size="1"><b><img src=.*/(\w+)\.jpg .*> (.+)</b></font></td>\s*' +
            '<td align="center" class="filets"><font color="#[\dA-F]+" size="1">(.+) - (.+)&nbsp;\s*</font></td>\s*' +
            '<td align="center" class="filets"><font color="#[\dA-F]+" size="1">(.+)&nbsp;\s*</font></td>\s*' +
            '<td align="center" nowrap class="filets">\s*<font color="#[\dA-F]+" size="1">\s*([\d:\.]+)&nbsp;</font></td>\s*' +
            '<td align="center" nowrap class="filets">\s*<font color="#[\dA-F]+" size="1">\s*([\d:\.]+)&nbsp;</font></td>\s*' +
            '<td align="center" nowrap class="filets">\s*<font color="#[\dA-F]+" size="1"><b>\s*([\d:\.]+)\s*&nbsp;</b></font></td>')
    try:
        content = urllib2.urlopen(url).read()
        results = map(convert, regex.findall(content))
        return results
    except:
        logging.exception('Could not get results')
        return ()


def roadbook(name):
    roadbook = ()
    if name:
        try:
            def convert_stage(stage):
                stage_name, rbr_stage_name, mod, weather, time_of_day = stage
                return RoadbookStage(_decode(stage_name), _decode(rbr_stage_name), _decode(mod), _decode(weather), None, _decode(time_of_day).capitalize(), None)

            # find id
            content = urllib2.urlopen(roadbook_list_url).read()
            rallies = re.findall('<option value="([1-9]\d*)">(.*) \(.*\) .*</option>', content)
            for nr, rally in rallies:
                if _decode(rally) == name:
                    # now nr is the roadbook id
                    content = urllib2.urlopen(roadbook_url + '?id=' + nr).read()
                    m = re.search('<img src=\'\s*(.+)\s*\' border=\'0\'>', content)
                    img_link = m.group(1) if m else ''
                    sdate = None
                    edate = None
                    m = re.search('<br>From\s+(\d\d)-(\d\d)\s+to\s+(\d\d)-(\d\d)\s*-\s*(\d\d\d\d)\s*<br>', content)
                    if m:
                        sday, smon, eday, emon, year = m.groups()
                        sdate = datetime.date(int(year), int(smon), int(sday))
                        edate = datetime.date(int(year), int(emon), int(eday))
                        if sdate > edate:
                            sdate = datetime.date(int(year) + 1, int(smon), int(sday))
                    else:
                        m = re.search('<br>\s*from\s+(\d\d)/(\d\d)/(\d\d\d\d)\s+to\s+(\d\d)/(\d\d)/(\d\d\d\d)\s*<br>', content)
                        if m:
                            sday, smon, syear, eday, emon, eyear = m.groups()
                            sdate = datetime.date(int(syear), int(smon), int(sday))
                            edate = datetime.date(int(eyear), int(emon), int(eday))
                    stages = re.findall('<br>\s*ES\s*:\s*\d+\s*<span style=" font-style: italic;">\s*([^\t<]+)\s*</span> /\s*([^\t]+)\t /\t(\w+)\t Mod / \t(\w+)\s*(?:/\s*(\w+)\s*)?', content)
                    roadbook = Roadbook(None, sdate, edate, img_link, map(convert_stage, stages))
                    break
        except:
            logging.exception('Could not get roadbook')
            pass
    return roadbook


def roadbook2(name, legs):
    roadbook = ()
    if name:
        try:
            def convert_stage(stage):
                # rally nr, rbr stage name, mod, weather, tyre change, night
                nr, rbr_stage_name, mod, weather, tyre_change, night = stage
                return RoadbookStage(None, _decode(rbr_stage_name), _decode(mod), _decode(weather), tyre_change == 'True', 'Night' if night == 'True' else 'Day', int(nr))

            all_stages = []
            if legs <= 0:
                legs = 1
            # rally nr, rbr stage name, mod, weather, tyre change, night
            regex = re.compile(
                '<tr bgcolor="#700000" class="filets">\s*' +
                '<td align="right" class="boutonclass"><font size="1">(\d+)&nbsp;&nbsp;</font></td>\s*' +
                '<td class="filets"><font size="1"><[^<>]+>&nbsp;[^<>]+</font></td>\s*' +
                '<td class="filets"><font size="1">([^<>]+)</font></td>\s*' +
                '<td class="filets"><font size="1">([^<>]+)</font></td>\s*' +
                '<td class="filets"><font size="1">([^<>]+)</font></td>\s*' +
                '<td class="filets"><font size="1">([^<>]+)</font></td>\s*' +
                '<td class="filets"><font size="1">([^<>]+)</font></td>\s*' +
                '</tr>')
            for leg in range(legs):
                n = name if legs == 1 else name + ' Leg ' + str(leg + 1)
                content = urllib2.urlopen(roadbook2_url + '?rallye=' + urllib.quote(n)).read()
                stages = re.findall(regex, content)
                if stages:
                    all_stages += stages
            roadbook = Roadbook(None, None, None, None, map(convert_stage, all_stages))
        except:
            logging.exception('Could not get roadbook')
            pass
    return roadbook

def roadbook_public(name, session_count):
    roadbook = ()
    if name:
        def convert_stage(stage):
            # rally nr, rbr stage name, mod, weather, tyre change, night
            nr, rbr_stage_name, mod, weather, tyre_change, night, day, month, year = stage
            return RoadbookStage(None, _decode(rbr_stage_name), _decode(mod), _decode(weather), tyre_change == 'True', 'Night' if night == 'True' else 'Day', int(nr))

        # rally nr, rbr stage name, mod, weather, tyre change, night, date/time start
        regex = re.compile(
            '<tr class="filets">\s*' +
            '<td align="right" nowrap class="boutonclass"><b><font size="1">(\d+)&nbsp;&nbsp;</font></b></td>\s*' +
            '<td class="filets"><font size="1"><img [^<>]+>&nbsp;[^<>]+</font></td>\s*' +
            '<td class="filets"><font size="1"><a [^<>]+>([^<>]+)</a></font></td>\s*' +
            '<td class="filets"><font size="1">([^<>]+)</font></td>\s*' +
            '<td class="filets"><font size="1">([^<>]+)</font></td>\s*' +
            '<td class="filets"><font size="1">([^<>]+)</font></td>\s*' +
            '<td class="filets"><font size="1">([^<>]+)</font></td>\s*' +
            '<td colspan="2" class="filets"><font size="1">(\d\d)/(\d\d)/(\d\d\d\d)\s+\d\d?:\d\d:\d\d</font></td>\s*' +
            '</tr>')
        for s in range(0, session_count + 1):
            try:
                n = s and name + str(s) or name
                content = urllib2.urlopen(public_roadbook_url + '?rallye=' + urllib.quote(n)).read()
                stages = re.findall(regex, content)
                if stages:
                    first_stage = stages[0]
                    sdate = datetime.date(int(first_stage[-1]), int(first_stage[-2]), int(first_stage[-3]))
                    last_stage = stages[-1]
                    edate = datetime.date(int(last_stage[-1]), int(last_stage[-2]), int(last_stage[-3]))
                    roadbook = Roadbook(None, sdate, edate, None, map(convert_stage, stages))
                    if roadbook:
                        break
            except:
                logging.exception('Could not get roadbook')
                pass
    return roadbook

def race_checks():
    """(url, [(rally_name, [driver id, ...]), ...])"""
    data = urllib.urlopen(news_url).read()
    results = re.findall('style="color:  darkblue;">([^<]+)</span></span></span><br>([^<&]+)(?:&nbsp;|<)??', data)
    
    def convert(result):
        def get_nr(driver):
            driver = driver.strip()
            if driver:
                m = re.search('(\d+)', driver)
                if m:
                    return m.group(1)
            return None
        rally, checks = result
        drivers = checks.split('/')
        return rally, filter(lambda nr: nr is not None, map(get_nr, drivers))
    
    results = map(convert, results)
    item_url = news_url
    m = re.search(r'itemid: #(\d+)', data)
    if m:
        item_url += '?id=%s' % m.group(1)
    return item_url, results


def parse_roadbook_csv(f):
    def parse_date(s):
        day, month, year = s.split('-')
        return datetime.date(int(year) + 2000, int(month), int(day))
    text = f.read().decode('iso-8859-1')
    lines = text.split('\n')
    lines = map(lambda line: line.split(';'), lines)
    rally_name = None
    sdate = None
    edate = None
    leg = 0
    tyres = False
    ss = 0
    ss_in_leg = 0
    stages = []
    for line in lines:
        if len(line) > 0:
            t = line[0]
            if t == 'MJN domein DUTCH OPEN':
                rally_name = line[5].strip()
            elif t == 'Week of race':
                sdate = parse_date(line[2])
            elif t == '' and sdate and not edate:
                edate = parse_date(line[2])
            elif t == 'LEG':
                leg += 1
                tyres = True
                ss_in_leg = 0
            elif t == 'TOOK PARK':
                tyres = True
            elif t == 'ESS':
                ss += 1
                ss_in_leg += 1
                track = line[2].strip()
                name = track + ' ' + line[4]
                mod = line[7]
                weather = line[10]
                daytime = line[13]
                stages.append(RoadbookStage(name, track, mod, weather, tyres, daytime, ss_in_leg))
                tyres = False

    return Roadbook(rally_name, sdate, edate, None, stages) if rally_name else None
