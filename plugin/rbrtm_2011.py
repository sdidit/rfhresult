#.csv:
#http://rbr.onlineracing.cz/tools.php?act=tour_stages_res_csv&torid=64fecbcbc69d628cc942bc5690bb1e22
#
#"pos";"user_name";"country";"car";"car_code";"stage_pos";"stagename";"time1";"time2";"time3";"timediff";"penalty";"finishrealtime";"restarts";"trackprogress";"weathercode";"tyrescode";"damagecode";"comment";
#1;DUCK tsute2;JP;Skoda Fabia WRC 05;264;0;Kaihuavaara;90.7064;170.075;222.82;0;0;1237131661;0;100;0;6;3;;
#2;JUSIAK Mariusz;PL;Skoda Fabia WRC 06;265;0;Kaihuavaara;92.7661;176.302;232.32;9.5;0;1237114476;0;100;0;6;3;;
#...
#DNF;KRPELAN Michal;SK;Peugeot 306 Maxi Evo 2;176;0;Kaihuavaara;108.109;0;0;0;0;1237123707;0;54.0221311475;0;6;3;;
#DNF;KUBICIK Josef;CZ;Toyota Celica st205;137;0;Kaihuavaara;96.5444;183.193;0;0;0;1237111832;0;81.3586885246;0;6;3;;
#1;owenmorgan23;GB;Citroen C4 WRC 08;278;1;Harwood Forest;67.5359;131.96;202.264;0;0;1237128260;0;100;0;3;3;;
#2;DUCK tsute2;JP;Skoda Fabia WRC 05;264;1;Harwood Forest;68.3478;132.955;203.7;1.436;0;1237131915;0;100;0;3;3;;
#...

#.html:
#http://rbr.onlineracing.cz/index.php?act=tourmntres2&torid=64fecbcbc69d628cc942bc5690bb1e22&stage=1&splits=1

url = 'http://rbr.onlineracing.cz/tools.php?act=tour_stages_res_csv'

# todo: get tournament id

def full_results_url(name, ss = 0):
    return None

def results(rally, ss = 0):
    return ()

def live_results(rally, ss = 0):
    return ()

def roadbook(name):
    return None

def roadbook2(name, legs):
    return None

def roadbook_public(name, session_count):
    return None
