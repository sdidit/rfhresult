import urllib
import urllib2
import logging
import re
import zlib

from google.appengine.api import urlfetch
from google.appengine.api import memcache

import lxml.html
from lxml.cssselect import CSSSelector
import settings
import time_util
import tools
from models import Result, Roadbook, RoadbookStage, STATE_FINISHED, STATE_RETIRED, STATE_SKIPPED, STATE_IN_PROGRESS


TIMEOUT_SECS = 300

domain = 'xv.rallyesim.com'

url_base = 'http://' + domain + '/'

news_url = url_base + 'mod_view.asp'  # pass id for specific news item, no id for latest, id=0 for empty (news selector combobox only)

rally_express_url = url_base + 'results_express.asp'
rally_official_url = url_base + 'results_rank_rally.asp'  # http://xv.rallyesim.com/results_rank_rally.asp?rallye=Rally_of_the_1000_Lakes&order=Time
rally_official_stage_url = url_base + 'results_stage.asp'  # http://xv.rallyesim.com/results_stage.asp?rallye=Rally_of_the_1000_Lakes&stage=1&Rank_View=0&order=Time
rally_live_url = url_base + 'classement_live.asp'
rally_live_stage_url = url_base + 'live.asp'
rally_live_index_url = url_base + 'index_live.asp'
roadbook_list_url = url_base + 'live_accueil.asp'
roadbook_url = url_base + 'roadbook.asp'
public_roadbook_url = url_base + 'public_index_live.asp'
public_rally_url = url_base + 'public_classement_live.asp'
public_stage_url = url_base + 'public_live.asp'
racecheck_url = url_base + 'live_rally_control_list.asp?week=%d'

# --- public session ----------------------------------------------------------------------#
#       roadbook: public_roadbook_url + '?rallye=RFH%20EVO%206%20TME%20Cup%20Gravel5'      #
# overall result: public_rally_url    + '?rallye=RFH%20EVO%206%20TME%20Cup%20Gravel5'      #
#   stage result: public_stage_url    + '?rallye=RFH%20EVO%206%20TME%20Cup%20Gravel5&ES=1' #
# -----------------------------------------------------------------------------------------#


# mmm:ss.hh (hours are not included)
time_regex = re.compile('(\d+):(\d\d)\.(\d\d)')

wrc2012_cars = {'Mini WRC', 'VW Polo WRC Concept', 'Ford Fiesta RS WRC 2012'}

# all packs not in this dict will have a class equal to first 2 characters of pack
pack_to_class = {
    'A8W': 'A8W',
    'RR1': 'R1',
    'RR2': 'R2',
    'RR3': 'R3',
    'RR4': 'R4',
    'RR5': 'R5',
    'RRC': 'RRC',
    'S2000': 'S2000',
    'S20004S': 'S2000',
    'N4S': 'S2000',
    'N4_S2000_1': 'S2000',
    'N4_S2000_2': 'S2000',
    'VHC0': 'VHC',
    'VHC1': 'VHC',
    'VHC2': 'VHC',
    'VHC3': 'VHC',
    'VHC4': 'VHC',
    'F200012': 'F2000_12',
    'F200013': 'F2000_13',
    'F200014': 'F2000_14',
    'B': 'Group_B',
    'B0': 'Group_B',
    'Group_B': 'Group_B',
    'H': 'Group_H',
    'H0': 'Group_H',
    'Group_H': 'Group_H',
}

country_to_code = {
    'pay': 'nl',
    'hog': 'hu',
    'all': 'de',
    'sui': 'ch',
    'dan': 'dk',
    'por': 'pt',
    'cri': 'hr',
    'thq': 'cz',
    'mad': 'mg',
    'and': 'ad',
    'mex': 'mx',
    'pol': 'pl',
    'tur': 'tr',
    'maq': 'mq',
    'slv': 'si',
    'eta': 'us',
    'gra': 'uk',
    'sue': 'se',
    'let': 'lv',
    'bul': 'bg',
    'cat': 'ct',
    'par': 'py',
    'isr': 'il',
    'bar': 'bb',
    'sri': 'lk',
    'jam': 'jm',
    'equ': 'ec',
    'lit': 'lt',
    'csr': 'cr',
}


def fetch(url, **kwargs):
    if kwargs:
        args = []
        for key, value in kwargs.iteritems():
            k = urllib.quote(key)
            v = value
            if isinstance(v, basestring):
                v = urllib.quote(value)
            args.append('%s=%s' % (k, v))
        url += '?' + '&'.join(args)
    return urlfetch.fetch(url, deadline=TIMEOUT_SECS, follow_redirects=False)


def _decode(s):
    return s.decode('latin-1')


def text(e):
    if len(e):
        return ' '.join(s.text_content() for s in e).strip()
    return e.text_content().strip()


def parse_car_class(pack, car):
    if car:
        if pack == 'A8W':
            if car.endswith('2011'):
                return pack + '-11'
            if car in wrc2012_cars or car.endswith('2012'):
                return pack + '-12'
            if car.endswith('2013'):
                return pack + '-13'
            if car.endswith('2014'):
                return pack + '-14'
            if car.endswith('2015'):
                return pack + '-15'
        elif pack == 'N4S':
            if car.endswith('RRC'):
                return 'RRC'
    return pack_to_class.get(pack) or pack[:2]


def parse_country(country):
    return country_to_code.get(country) or country[:2]


def parse_time(s):
    try:
        m = time_regex.match(s)
        if not m:
            return time_util.INVALID_TIME
        return (int(m.group(1)) * 60 + int(m.group(2))) * 100 + int(m.group(3))
    except Exception, e:
        logging.exception('Could not parse time "%s": %s' % (s, str(e)))
        return time_util.INVALID_TIME

status_dict = {'finished': STATE_FINISHED,
               'retired': STATE_RETIRED,
               'skipped': STATE_SKIPPED,
               'in progress': STATE_IN_PROGRESS}


def parse_status(status):
    return status_dict.get(status.strip().lower()) or STATE_FINISHED


def parse_official_status(status):
    return STATE_FINISHED if not status or status in ('-', 'Finished') else STATE_RETIRED


def official_url(name, ss=0):
    n = name.replace(' ', '_')
    if ss:
        return '%s?rallye=%s&stage=%d&Rank_View=0&order=Time' % (rally_official_url, ss, n)
    return '%s?rallye=%s&order=Time&Rank_View=3' % (rally_official_url, n)


def official_results(name, ss=0):
    try:
        key = official_url(name, ss)
        content = memcache.get(key)
        if content is None:
            result = fetch(key)
            if result.status_code == 200:
                content = result.content
                memcache.add(key, zlib.compress(content), time_util.SECS_IN_HOUR)
            else:
                logging.error('Could not get results: http error ' + str(result.status_code))
        else:
            content = zlib.decompress(content)

        doc = lxml.html.fromstring(content)
        items = doc.cssselect('table[bgcolor="#FFFFFF"] tr.trtxtblack')
        selcolumns = CSSSelector('td')
        selname = CSSSelector('b')
        selcar = CSSSelector('font:nth-child(3)')
        selflag = CSSSelector('img')
        flagre = re.compile(r'/(\w+)\.jpg')

        def convert(e):
            columns = selcolumns(e)
            count = len(columns)
            it = iter(columns)
            it.next()  # skip pos
            plugin_id = text(it.next())
            name = text(selname(it.next()))
            car = text(selcar(e)[0])
            flag = selflag(e)[0].get('src')
            country = flagre.search(flag)
            country = parse_country(country.group(1)) if country else ''
            it.next()  # skip team
            if ss == 0:
                if count == 12:
                    it.next()  # skip stable
                    it.next()  # skip group
                it.next()  # skip class
                split1 = time_util.INVALID_TIME
                split2 = time_util.INVALID_TIME
            else:
                if count == 14:
                    it.next()  # skip stable
                    it.next()  # skip group
                it.next()  # skip class
                split1 = parse_time(text(it.next()))
                split2 = parse_time(text(it.next()))
            time = parse_time(text(it.next()))
            it.next()  # skip diff leader
            it.next()  # skip diff prev
            it.next()  # skip points
            status = parse_official_status(text(it.next()))
            return Result(plugin_id, name, country, None, car, split1, split2, time, 0, status)

        results = map(convert, items)
        if results:
            express = express_results(name, ss)
            express_map = {}
            for result in express:
                express_map[result.plugin_id] = result
            for result in results:
                er = express_map.get(result.plugin_id)
                if er:
                    result.car_class = er.car_class
        return results
    except Exception, e:
        logging.exception('Could not get results: ' + str(e))
    return ()


def full_results_url(name, ss=0):
    return express_url(name, ss)


def express_url(name, ss=0):
    stage = str(ss) if ss else 'All'
    params = {'cmbNumero': name, 'cmbNumber': stage, 'cmbAfter': 'All', 'Submit': 'Send'}
    return rally_express_url + '?' + urllib.urlencode(params)


def express_results(name, ss=0):
    try:
        key = express_url(name, ss)
        content = memcache.get(key)
        if content is None:
            result = fetch(key)
            if result.status_code == 200:
                content = result.content
                memcache.add(key, zlib.compress(content), time_util.SECS_IN_HOUR)
            else:
                logging.error('Could not get results: http error ' + str(result.status_code))
                return ()
        else:
            content = zlib.decompress(content)

        doc = lxml.html.fromstring(content)
        items = doc.cssselect('table[bgcolor="#FFFFFF"] tr.trtxtblacksz1')[1:]
        selcolumns = CSSSelector('td')
        selflag = CSSSelector('img')
        flagre = re.compile(r'/(\w+)\.jpg')

        def convert(e):
            columns = selcolumns(e)
            it = iter(columns)
            it.next()  # skip pos
            plugin_id = text(it.next())
            name_elem = it.next()
            name = text(name_elem)
            flag = selflag(name_elem[0])[0].get('src')
            country = flagre.search(flag)
            country = parse_country(country.group(1)) if country else ''
            car = text(it.next())
            group = text(it.next())
            cls = text(it.next())
            pack = group + cls
            car_class = parse_car_class(pack, car)
            if ss == 0:
                split1 = time_util.INVALID_TIME
                split2 = time_util.INVALID_TIME
            else:
                split1 = parse_time(text(it.next()))
                split2 = parse_time(text(it.next()))
            time = parse_time(text(it.next()))
            it.next()  # skip diff leader
            it.next()  # skip diff prev
            status = parse_official_status(text(it.next()))
            return Result(plugin_id, name, country, car_class, car, split1, split2, time, 0, status)

        express = map(convert, items)
        memcache.add(key, express, time_util.SECS_IN_HOUR)
        return express
    except Exception, e:
        logging.exception('Could not get results: ' + str(e))
    return ()


def single_public_result(rally, session, ss):
    # plugin_id, country/name, time, diff_leader, diff_prev
    name = rally.get_import_name(session)
    if ss == 0:
        url = '%s?rallye=%s' % (public_rally_url, urllib.quote(name))
    else:
        url = '%s?rallye=%s&ES=%d' % (public_stage_url, urllib.quote(name), ss)
    try:
        content = urllib2.urlopen(url).read()
        doc = lxml.html.fromstring(content)
        if ss == 0:
            items = doc.cssselect('tr[bgcolor="#FFFFFF"]')[3:]
        else:
            items = doc.cssselect('tr[bgcolor="FFFFFF"]')   # bug in stage results: no '#'
            if not items:
                # bug might have been fixed
                items = doc.cssselect('tr[bgcolor="#FFFFFF"]')[3:]
        selcolumns = CSSSelector('td')
        selflag = CSSSelector('img')
        flagre = re.compile(r'/(\w+)\.jpg')
        modpackre = ss != 0 and re.compile(r'.*\s-\s(.*)')
        default_car = rally.champ.fixed_car
        default_pack = default_car and rally.champ.car_classes[0]

        def convert(e):
            columns = selcolumns(e)
            it = iter(columns)
            it.next()  # skip pos
            plugin_id = text(it.next())
            name_elem = it.next()
            country = flagre.search(selflag(name_elem[0])[0].get('src'))
            country = parse_country(country.group(1)) if country else ''
            name = text(name_elem)
            if ss == 0:
                pack = default_pack
                car = default_car
                split1 = time_util.INVALID_TIME
                split2 = time_util.INVALID_TIME
            else:
                modpack = modpackre.search(text(it.next()))
                car = text(it.next()) or default_car
                pack = parse_car_class(modpack.group(1), car) if modpack else default_pack
                split1 = parse_time(text(it.next()))
                split2 = parse_time(text(it.next()))
            time = parse_time(text(it.next()))
            if ss == 0:
                status = STATE_FINISHED
            else:
                it.next()  # skip diff to leader
                it.next()  # skip diff to prev
                status = parse_status(text(it.next()))
            return Result(plugin_id, name, parse_country(country), pack, car, split1, split2, time, session, status)

        return map(convert, items)
    except:
        logging.exception('Could not get results')
        return ()


def merge_first_result(final_result, result):
    for r in result:
        pid = r.plugin_id
        # first session done by driver is the one that counts, no retries
        if pid not in final_result:
            final_result[pid] = r


def merge_last_result(final_result, result):
    for r in result:
        pid = r.plugin_id
        # last session done by driver is the one that counts
        final_result[pid] = r


def merge_best_result(final_result, result):
    for r in result:
        pid = r.plugin_id
        # best session done by driver is the one that counts
        old_result = final_result.get(pid)
        if old_result is None or old_result.t3 > r.t3:
            final_result[pid] = r


def determine_merge(rally):
    if rally.is_use_first_result():
        merge = merge_first_result
    elif rally.is_use_last_result():
        merge = merge_last_result
    else:
        merge = merge_best_result
    return merge


def fix_car(final_result, rally, ss):
    # public overall result lacks pack and car (unless there's a fixed car), get it from first stage result
    if ss == 0 and not rally.champ.fixed_car:
        session_ss1_results = {}  # cache
        for pid, r in final_result.iteritems():
            if not r.car:
                ss1_result = session_ss1_results.get(r.session)
                if ss1_result is None:
                    raw_ss1_result = single_public_result(rally, r.session, 1)
                    ss1_result = {}
                    for ss1_r in raw_ss1_result:
                        ss1_result[ss1_r.plugin_id] = ss1_r
                    session_ss1_results[r.session] = ss1_result
                ss1_driver_result = ss1_result.get(pid)
                if ss1_driver_result:
                    r.car = ss1_driver_result.car
                    r.car_class = ss1_driver_result.car_class


def public_results(rally, ss=0):
    final_result = {}
    merge = determine_merge(rally)
    for session in range(1, rally.session_count + 1):
        result = single_public_result(rally, session, ss)
        merge(final_result, result)
    fix_car(final_result, rally, ss)
    return sorted(final_result.values())


def results(rally, ss=0):
    if rally.champ.is_official():
        return official_results(rally.get_import_name(), ss)
    if rally.champ.is_unofficial():
        return express_results(rally.get_import_name(), ss)
    if rally.champ.is_public():
        return public_results(rally, ss)
    return ()


def live_results(rally, ss=0):
    if rally.champ.is_public():
        return public_results(rally, ss)
    return ()


def parse_roadbook(html, name):
    doc = lxml.html.fromstring(html)

    img_elems = doc.cssselect('table:nth-child(3) img')
    img_link = img_elems[0].get('src') if img_elems else None

    week_elems = doc.cssselect("i:contains('Week :') b")
    if not week_elems:
        raise ValueError('No week number found')

    week = int(week_elems[-1].text)
    year = settings.YEAR - 1 if week >= 51 else settings.YEAR
    sdate, edate = time_util.week_boundaries(year, week)

    leg1_table_header = doc.cssselect('table.trtxtblacksz1:nth-child(2) tr:nth-child(2) td')
    if not leg1_table_header:
        raise ValueError('No legs found')

    column_count = len(leg1_table_header) + 1
    has_weather = column_count == 8
    stage_elems = tools.grouper(column_count, doc.cssselect('.filets .filets'))

    def convert_stage(e):
        columns = [i.text_content().strip(u' \xa0') for i in e]
        it = iter(columns)
        it.next()  # skip nr
        it.next()  # skip country
        full_name = it.next()
        rbr_stage_name = full_name.split(u'\xa0')[1]
        if has_weather:
            mod, grip = it.next().split(u'\xa0')
        else:
            mod = it.next()
            grip = None
        length = it.next()
        stage_name = u'%s (%s)' % (full_name.replace(u'\xa0', u' / '), length)
        if mod.lower().endswith(u' mod'):
            mod = mod[:-4]
        if has_weather:
            weather = it.next()
            wear = it.next()
        else:
            weather = None
            wear = None
        time_of_day = it.next()
        nr_in_leg = None
        tyre_change = None
        return RoadbookStage(stage_name, rbr_stage_name, mod, weather, tyre_change, time_of_day.capitalize(), nr_in_leg,
                             wear, grip)

    stages = [convert_stage(e) for e in stage_elems]
    if len(stages) == 0:
        raise ValueError('No stages found')

    leg_elems = doc.cssselect('tr:nth-child(3) .filets b:nth-child(1)')
    leg_stages = {int(i.text_content()) - 1 for i in leg_elems}
    li = 1
    for i, stage in enumerate(stages):
        if i in leg_stages:
            li = 1
        stage.nr_in_leg = li
        li += 1

    service_elems = doc.cssselect("tr:contains('SERVICE PARK') + tr .filets b:nth-child(1)")
    tyre_stages = leg_stages | {int(i.text_content()) - 1 for i in service_elems}
    for i, stage in enumerate(stages):
        stage.tyre_change = i in tyre_stages

    return Roadbook(name, sdate, edate, img_link, stages)


def roadbook_names(all_time=False):
    result = fetch(roadbook_list_url)
    if result.status_code == 200:
        doc = lxml.html.fromstring(result.content)
        elems = doc.cssselect('.yellink:nth-child(2)' if all_time else '.tabletxtwhite .yellink')

        def convert(e):
            s = e.get('href')
            return re.match(r'roadbook.asp\?rallye=(.*)', s).group(1)

        return map(convert, elems)
    return []


def roadbook(name):
    roadbook = None
    if name:
        try:
            result = fetch(roadbook_url, rallye=name)
            if result.status_code == 200:
                roadbook = parse_roadbook(result.content, name)
        except:
            logging.exception('Could not get roadbook of ' + name)
    return roadbook


# noinspection PyUnusedLocal
def roadbook2(name, legs):
    roadbook = None
    return roadbook


def roadbook_public(name, session_count):
    roadbook = None
    if name:
        for s in range(0, session_count + 1):
            try:
                n = s and name + str(s) or name
                content = urllib2.urlopen(public_roadbook_url + '?rallye=' + urllib.quote(n)).read()
                # todo: public session uses different roadbook format
                rb = parse_roadbook(content, name)
                roadbook = rb
                if roadbook:
                # todo: parse start/end date: (\d\d)/(\d\d)/(\d\d\d\d)\s+\d\d?:\d\d:\d\d
                # if roadbook.sdate is None:
                #      first_stage = roadbook.stages[0]
                #      roadbook.sdate = datetime.date(int(first_stage[-1]), int(first_stage[-2]), int(first_stage[-3]))
                #  if roadbook.edate is None:
                #      last_stage = roadbook.stages[-1]
                #      roadbook.edate = datetime.date(int(last_stage[-1]), int(last_stage[-2]), int(last_stage[-3]))
                    break
            except:
                logging.exception('Could not get roadbook')
                pass
    return roadbook


def race_checks(weeknr=0):
    """(url, [(rally_name, [driver id, ...]), ...])"""
    if weeknr == 0:
        # last week
        weeknr = time_util.date_to_iso_week(time_util.date_offset(time_util.today(), -7))
    url = racecheck_url % weeknr
    data = urllib2.urlopen(url).read()
    doc = lxml.html.fromstring(data)
    lines = [line.text_content().strip() for line in doc.cssselect('.filets')[1:]]

    checks = []
    rally = None
    drivers = []
    for line in lines:
        if rally is None:
            rally = line
        else:
            if len(line) > 0:
                if line.isdigit():
                    drivers.append(line)  # driver id
                elif drivers and (line.endswith('received') or line.endswith('DNF')):
                    drivers.pop()
            else:
                checks.append((rally, drivers))
                rally = None
                drivers = []
    if rally:
        checks.append((rally, drivers))

    return url, checks


# noinspection PyUnusedLocal
def parse_roadbook_csv(f):
    # it was decided to import the roadbook from RSRBR directly
    return None
