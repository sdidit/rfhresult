background_color = '000000'
sub_colors = ('3042fb', '76a4fb', '881be0', 'e0601b')
legenda_colors = ('ffeebb', 'ff0000', '00ff00', '0000ff', 'c0c000', '00ffff', 'ff00ff', '8000ff', 'ff8000', '808080')


class ChartItem(object):
    def __init__(self, name, values):
        self.name = name.encode('utf-8', 'ignore')
        self.values = values

    def string_values(self):
        return (str(value) for value in self.values)


def _build_chart_url(params):
    return 'https://chart.googleapis.com/chart?' + '&'.join('='.join(param) for param in params)


def create_bar_chart(items, width=600, tick_round=-2):
    # https://chart.googleapis.com/chart
    # ?chxt=x,y&cht=bvs&chd=s:cEj9U&chco=76A4FB&chls=2.0&chs=200x125&chxl=0:|Jan|Feb|Mar|Apr|May
    params = []
    params.append(('chxt', 'x,y'))
    params.append(('cht', 'bhs'))  # bar, horizontal, stacked
    params.append(('chf', 'bg,s,' + background_color))  # solid background fill
    params.append(('chm', 'N,000000,-1,-1,11'))  # data label
    params.append(('chxs', '0,C0C0C0,11.5,0,l,C0C0C0|1,C0C0C0,11.5,0,lt,C0C0C0'))  # style
    sub_count = len(items[0].values) if items else 0
    if sub_count:
        data = '|'.join((','.join(str(item.values[i]) for item in items)) for i in range(sub_count))
        params.append(('chd', 't:' + data))
        end_val = str(round(sum(items[0].values) + (10 ** (-tick_round)), tick_round))
        params.append(('chxr', '0,0,' + end_val))  # range
        params.append(('chds', '0,' + end_val))  # scale
        params.append(('chco', ','.join(sub_colors[:sub_count])))
    params.append(('chls', '2.0'))
    params.append(('chs', str(width) + 'x' + str(30 + 27 * len(items))))  # size in pixels, bar is 24px, spacing is 3px
    names = '|'.join(item.name for item in reversed(items))
    params.append(('chxl', '1:|' + names))  # labels
    return _build_chart_url(params)


def create_line_chart(items, labels=None, width=800, height=375, tick_round=-1):
    # https://chart.googleapis.com/chart
    # ?cht=ls&chd=t:0,30,60,70,90,95,100|20,30,40,50,60,70,80|10,30,40,45,52
    # &chco=ff0000,00ff00,0000ff&chs=250x150&chdl=NASDAQ|FTSE100|DOW
    params = []
    params.append(('chxt', 'x,y'))
    params.append(('cht', 'lc'))  # line
    params.append(('chf', 'bg,s,' + background_color))  # solid background fill
    params.append(('chdlp', 't'))  # legend at top
    params.append(('chxs', '0,C0C0C0,11.5,0,l,C0C0C0|1,C0C0C0,11.5,0,lt,C0C0C0'))  # style
    if labels:
        params.append(('chxl', '0:|' + '|'.join(label.encode('utf-8', 'ignore') for label in labels)))
    if items:
        sub_count = len(items[0].values)
        if sub_count > 0:
            if sub_count > 1:
                params.append(('chg', str(100.0 / (sub_count - 1)) + ',0'))
            end_val = str(round(items[0].values[-1] + (10 ** (-tick_round)), tick_round))
            params.append(('chxr', '0,1,' + str(sub_count) + '|1,0,' + end_val))
            params.append(('chds', '0,' + end_val))  # scale
            data = '|'.join((','.join(item.string_values())) for item in items)
            params.append(('chd', 't:' + data))
            params.append(('chco', ','.join(legenda_colors[:len(items)])))  # legenda colors
            names = '|'.join(item.name for item in items)
            params.append(('chdl', names))  # labels
    params.append(('chs', str(width) + 'x' + str(height)))  # size in pixels
    return _build_chart_url(params)
