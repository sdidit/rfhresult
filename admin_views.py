from google.appengine.ext import webapp
from google.appengine.api import memcache
from google.appengine.api import mail
from google.appengine.ext.webapp import template

import settings
import models
import admin_forms
import context
import time_util


ADMIN_URL = '/admin/'

REPLAY_REMINDER_MAIL_BODY = """
Beste %s,

Je dient je replays op te sturen voor de %s rally!

Zie %s

Groeten van mijnrally.nl Rally Center

PS: deze e-mail is automatisch gegenereerd
"""


RESULTS_REMINDER_MAIL_BODY = """
The results of the %s rally need to be imported!

See %s
"""


# def notify_result_manager_added(user):
#     from google.appengine.api import mail
#     mail.send_mail(sender='sdidit@gmail.com',
#                    to="mijnrally.nl Rally Center Admin <sdidit@gmail.com>",
#                    subject="[mijnrally.nl] A result manager has been added",
#                    body="%s with email address %s has been added as a result manager.\n" % (user, user.email()))
#
#
# def notify_result_manager_deleted(user):
#     from google.appengine.api import mail
#     mail.send_mail(sender='sdidit@gmail.com',
#                    to="mijnrally.nl Rally Center Admin <sdidit@gmail.com>",
#                    subject="[mijnrally.nl] A result manager has been deleted",
#                    body="%s with email address %s has been deleted as a result manager.\n" % (user, user.email()))


def create_props(request, more_props):
    p = models.base_props(request)
    p.update(more_props)
    return p


def stat_props():
    stats = memcache.get_stats()
    if stats is None:
        stats = {}
    else:
        hits = stats['hits']
        misses = stats['misses']
        total = hits + misses
        if total:
            hit_ratio = (hits * 100) / total
        else:
            hit_ratio = 0
        stats['hit_ratio'] = hit_ratio
        se = int(stats['oldest_item_age'])
        mi = se / 60
        ho = mi / 60
        da = ho / 24
        stats['oldest_item_age_days'] = da
        stats['oldest_item_age_hours'] = ho % 24
        stats['oldest_item_age_minutes'] = mi % 60
    return stats


def obj_url(obj_id, name):
    if obj_id:
        return '%s%s/%s/' % (ADMIN_URL, name, obj_id)
    return '%s%s/' % (ADMIN_URL, name)


def team_url(team_id=None):
    return obj_url(team_id, 'team')


def champ_url(champ_id=None):
    return obj_url(champ_id, 'champ')


def rally_url(rally_id=None):
    return obj_url(rally_id, 'rally')


def member_url(member_id=None):
    return obj_url(member_id, 'member')


def news_url(news_item_id=None):
    return obj_url(news_item_id, 'news')


# base form for admin forms
class AdminFormHandler(webapp.RequestHandler):
    def render_form(self, form, tmpl, title, cancel_url=''):
        self.response.out.write(template.render(
            settings.admin_template_path(tmpl), create_props(self.request, {
                'title': title,
                'form': form,
                'cancel_url': ADMIN_URL + cancel_url,
            })))


class AdminHandler(webapp.RequestHandler):
    def get(self):
        props = create_props(self.request, {
            'title': 'Admin Console',
        })
        props.update(stat_props())
        self.response.out.write(template.render(
            settings.admin_template_path('admin.html'), props))


class FlushCacheHandler(webapp.RequestHandler):
    def get(self):
        memcache.flush_all()
        self.redirect(ADMIN_URL)


class CheckRacesHandler(webapp.RequestHandler):
    def get(self):
        context.perform_race_checks(settings.ADMIN_EMAIL, REPLAY_REMINDER_MAIL_BODY, self.request.host_url)


class CheckResultsHandler(webapp.RequestHandler):
    def get(self):
        rallies = models.Rally.all().filter('end_date =', time_util.yesterday())
        for rally in rallies:
            if not rally.are_results_imported():
                mail.send_mail(sender=settings.ADMIN_EMAIL,
                               to=settings.ADMIN_EMAIL,
                               subject="Import results of %s" % rally,
                               body=RESULTS_REMINDER_MAIL_BODY % (rally.name, rally_url(rally.key())))


class ImportRalliesHandler(webapp.RequestHandler):
    def get(self):
        context.import_rallies()

##class ResultManagerListHandler(webapp.RequestHandler):
##    def get(self):
##        self.response.out.write(template.render(
##            settings.admin_template_path('admin_manager_list.html'), create_props(self.request, {
##                'title': 'Admin Console - Result Manager List',
##                'result_managers': models.ResultManager.all().order('user'),
##            })))
##
##class AddResultManagerHandler(AdminFormHandler):
##    def render_form(self, form):
##        super(AddResultManagerHandler, self).render_form(form, 'admin_manager_add.html', 'Add Result Manager')
##    def get(self):
##        self.render_form(admin_forms.ResultManagerForm())
##    def post(self):
##        form = admin_forms.ResultManagerForm(self.request.POST)
##        if form.is_valid():
##            obj = form.save()
##            notify_result_manager_added(obj.user)
##            self.redirect('/admin/manager/')
##        else:
##            self.render_form(form)
##
##class DeleteResultManagerHandler(webapp.RequestHandler):
##    def post(self, obj_id):
##        obj = models.ResultManager.get(obj_id)
##        if obj:
##            obj.delete()
##            notify_result_manager_deleted(obj.user)
##        self.redirect('/admin/manager/')


class MemberListHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write(template.render(
            settings.admin_template_path('admin_member_list.html'), create_props(self.request, {
                'title': 'Admin Console - Member List',
                'members': models.Member.all().order('name'),
            })))


class ImportMembersHandler(AdminFormHandler):
    def post(self):
        members_csv = self.request.POST.get('members')
        if members_csv:
            for m in members_csv.splitlines():
                columns = m.split(u'\t')
                if len(columns) >= 3:
                    member = models.Member.all().filter('name =', columns[0]).get()
                    if not member:
                        member = models.Member(name=columns[0])
                    if columns[1] and columns[1] != 'None':
                        member.email_address = columns[1]
                    if columns[2] and columns[2] != 'None':
                        member.rsrbr_id = columns[2]
                    if len(columns) >= 4 and columns[3] and columns[3] != 'None':
                        member.country = columns[3]
                    member.put()
        self.redirect(member_url())


class AddMemberHandler(AdminFormHandler):
    def render_member_form(self, form):
        super(AddMemberHandler, self).render_form(form, 'admin_generic_add.html', 'Add Member', 'member/')

    def get(self):
        self.render_member_form(admin_forms.MemberForm())

    def post(self):
        form = admin_forms.MemberForm(self.request.POST)
        if form.is_valid():
            form.save()
            self.redirect(member_url())
        else:
            self.render_member_form(form)


class EditMemberHandler(AdminFormHandler):
    def render_member_form(self, form):
        super(EditMemberHandler, self).render_form(form, 'admin_generic_edit.html', 'Edit Member', 'member/')

    def get(self, member_id):
        self.render_member_form(admin_forms.MemberForm(instance=models.Member.get(member_id)))

    def post(self, member_id):
        form = admin_forms.MemberForm(self.request.POST, instance=models.Member.get(member_id))
        if form.is_valid():
            form.save()
            self.redirect(member_url())
        else:
            self.render_member_form(form)


class DeleteMemberHandler(webapp.RequestHandler):
    def get(self, member_id):
        member = models.Member.get(member_id)
        self.response.out.write(template.render(
            settings.admin_template_path('admin_generic_delete.html'), create_props(self.request, {
                'title': 'Admin Console - Delete Member',
                'name': member.name,
                'cancel_url': member_url(str(member.key())),
            })))

    def post(self, member_id):
        member = models.Member.get(member_id)
        if member:
            member.delete()
        self.redirect(member_url())


class TeamListHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write(template.render(
            settings.admin_template_path('admin_team_list.html'), create_props(self.request, {
                'title': 'Admin Console - Team List',
                'teams': models.Team.all().order('name'),
            })))


class AddTeamHandler(AdminFormHandler):
    def render_team_form(self, form):
        super(AddTeamHandler, self).render_form(form, 'admin_generic_add.html', 'Add Team', 'team/')

    def get(self):
        self.render_team_form(admin_forms.TeamForm())

    def post(self):
        form = admin_forms.TeamForm(self.request.POST)
        if form.is_valid():
            form.save()
            self.redirect(team_url())
        else:
            self.render_team_form(form)


class EditTeamHandler(webapp.RequestHandler):
    def render_form(self, form):
        self.response.out.write(template.render(
            settings.admin_template_path('admin_team_edit.html'), create_props(self.request, {
                'title': 'Edit Team',
                'form': form,
                'team_members': form.instance.teammember_set,
                'image_width': models.TEAM_IMAGE_WIDTH,
                'image_height': models.TEAM_IMAGE_HEIGHT,
            })))

    def get(self, team_id):
        self.render_form(admin_forms.TeamForm(instance=models.Team.get(team_id)))

    def post(self, team_id):
        form = admin_forms.TeamForm(self.request.POST, instance=models.Team.get(team_id))
        if form.is_valid():
            form.save()
            self.redirect(team_url())
        else:
            self.render_form(form)


class DeleteTeamHandler(webapp.RequestHandler):
    def get(self, team_id):
        team = models.Team.get(team_id)
        self.response.out.write(template.render(
            settings.admin_template_path('admin_generic_delete.html'), create_props(self.request, {
                'title': 'Admin Console - Delete Team',
                'name': team.name,
                'cancel_url': team_url(str(team.key())),
            })))

    def post(self, team_id):
        team = models.Team.get(team_id)
        if team:
            context.flush_team_standings(team)
            team.delete()
        self.redirect(team_url())


class TeamBannerHandler(webapp.RequestHandler):
    def get(self, team_id):
        team = models.Team.get(team_id)
        if team.banner:
            self.response.headers['Content-Type'] = models.TEAM_IMAGE_CONTENT_TYPE
            self.response.out.write(team.banner)


class TeamBannerImportHandler(webapp.RequestHandler):
    def post(self, team_id):
        team = models.Team.get(team_id)
        data = self.request.POST.get('image')
        if data is not None and data != u'':
            if team.import_banner(data.file.read()):
                team.put()
        else:
            # clear image
            team.banner = None
            team.put()
        self.redirect(team_url(team_id))


class TeamSubFormHandler(webapp.RequestHandler):
    def render_form(self, form, team, tmpl, title):
        self.response.out.write(template.render(
            settings.admin_template_path(tmpl), create_props(self.request, {
                'title': title,
                'form': form,
                'team': team,
            })))


class AddTeamMemberHandler(TeamSubFormHandler):
    def render_teammember_form(self, form, team):
        super(AddTeamMemberHandler, self).render_form(form, team, 'admin_team_member_add.html', 'Add Team Member')

    def get(self, team_id):
        team = models.Team.get(team_id)
        self.render_teammember_form(admin_forms.TeamMemberForm(), team)

    def post(self, team_id):
        team = models.Team.get(team_id)
        form = admin_forms.TeamMemberForm(self.request.POST, instance=models.TeamMember(team=team))
        if form.is_valid():
            form.save()
            context.flush_team_standings(team)
            self.redirect(team_url(team_id))
        else:
            self.render_teammember_form(form, team)


class DeleteTeamMemberHandler(AdminFormHandler):
    def get(self, team_member_id):
        team_member = models.TeamMember.get(team_member_id)
        self.response.out.write(template.render(
            settings.admin_template_path('admin_generic_delete.html'), create_props(self.request, {
                'title': 'Admin Console - Delete Team Member',
                'name': unicode(team_member),
                'cancel_url': obj_url(str(team_member.key()), 'teammember'),
            })))

    def post(self, team_member_id):
        team_member = models.TeamMember.get(team_member_id)
        team_id = None
        if team_member:
            team_id = str(team_member.team.key())
            context.flush_team_standings(team_member.team)
            team_member.delete()
        self.redirect(team_url(team_id))


class EditTeamMemberHandler(TeamSubFormHandler):
    def render_teammember_form(self, form):
        super(EditTeamMemberHandler, self).render_form(form, form.instance.team, 'admin_team_member_edit.html',
                                                       'Edit Team Member')

    def get(self, team_member_id):
        self.render_teammember_form(admin_forms.TeamMemberForm(instance=models.TeamMember.get(team_member_id)))

    def post(self, team_member_id):
        team_member = models.TeamMember.get(team_member_id)
        form = admin_forms.TeamMemberForm(self.request.POST, instance=team_member)
        if form.is_valid():
            form.save()
            team_id = str(team_member.team.key())
            self.redirect(team_url(team_id))
        else:
            self.render_teammember_form(form)


class AddTeamCupHandler(AdminFormHandler):
    def render_teamcup_form(self, form):
        super(AddTeamCupHandler, self).render_form(form, 'admin_generic_add.html', 'Add Team Cup', 'champ/')

    def get(self):
        self.render_teamcup_form(admin_forms.TeamCupForm())

    def post(self):
        form = admin_forms.TeamCupForm(self.request.POST)
        if form.is_valid():
            form.save()
            self.redirect(champ_url())
        else:
            self.render_teamcup_form(form)


class EditTeamCupHandler(AdminFormHandler):
    def render_teamcup_form(self, form):
        super(EditTeamCupHandler, self).render_form(form, 'admin_generic_edit.html', 'Edit Team Cup', 'champ/')

    def get(self, team_cup_id):
        self.render_teamcup_form(admin_forms.TeamCupForm(instance=models.TeamCup.get(team_cup_id)))

    def post(self, team_cup_id):
        form = admin_forms.TeamCupForm(self.request.POST, instance=models.TeamCup.get(team_cup_id))
        if form.is_valid():
            form.save()
            self.redirect(champ_url())
        else:
            self.render_teamcup_form(form)


class DeleteTeamCupHandler(AdminFormHandler):
    def get(self, team_cup_id):
        team_cup = models.TeamCup.get(team_cup_id)
        self.response.out.write(template.render(
            settings.admin_template_path('admin_generic_delete.html'), create_props(self.request, {
                'title': 'Admin Console - Delete Team Cup',
                'name': team_cup.name,
                'cancel_url': rally_url(str(team_cup.key())),
            })))

    def post(self, team_cup_id):
        team_cup = models.TeamCup.get(team_cup_id)
        if team_cup:
            context.flush_team_standings()
            team_cup.delete()
        self.redirect(champ_url())


class ChampListHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write(template.render(
            settings.admin_template_path('admin_champ_list.html'), create_props(self.request, {
                'title': 'Admin Console - Championship List',
                'champs': models.Champ.all().order('name'),
                'teamcups': models.TeamCup.all().order('name'),
            })))


class AddChampHandler(AdminFormHandler):
    def render_champ_form(self, form):
        super(AddChampHandler, self).render_form(form, 'admin_generic_add.html', 'Add Championship', 'champ/')

    def get(self):
        self.render_champ_form(admin_forms.ChampForm())

    def post(self):
        form = admin_forms.ChampForm(self.request.POST)
        if form.is_valid():
            champ = form.save()
            champ.generate_participants()
            champ_id = str(champ.key())
            self.redirect(champ_url(champ_id))
        else:
            self.render_champ_form(form)


class EditChampHandler(webapp.RequestHandler):
    def render_form(self, form):
        self.response.out.write(template.render(
            settings.admin_template_path('admin_champ_edit.html'), create_props(self.request, {
                'title': 'Edit Championship',
                'form': form,
                'rallies': form.instance.rallies(),
                'drivers': form.instance.driver_set.order('plugin_id'),
            })))

    def get(self, champ_id):
        self.render_form(admin_forms.ChampForm(instance=models.Champ.get(champ_id)))

    def post(self, champ_id):
        champ = models.Champ.get(champ_id)
        form = admin_forms.ChampForm(self.request.POST, instance=champ)
        if form.is_valid():
            form.save()
            context.flush_standings(champ)
            self.redirect(champ_url())
        else:
            self.render_form(form)


class DeleteChampHandler(webapp.RequestHandler):
    def get(self, champ_id):
        champ = models.Champ.get(champ_id)
        self.response.out.write(template.render(
            settings.admin_template_path('admin_generic_delete.html'), create_props(self.request, {
                'title': 'Admin Console - Delete Championship',
                'name': champ.name,
                'cancel_url': champ_url(str(champ.key())),
            })))

    def post(self, champ_id):
        champ = models.Champ.get(champ_id)
        if champ:
            champ.delete()
        self.redirect(champ_url())


class ChampSubFormHandler(webapp.RequestHandler):
    def render_form(self, form, champ, tmpl, title):
        self.response.out.write(template.render(
            settings.admin_template_path(tmpl), create_props(self.request, {
                'title': title,
                'form': form,
                'champ': champ,
            })))


class AddRallyHandler(ChampSubFormHandler):
    def render_rally_form(self, form, champ):
        super(AddRallyHandler, self).render_form(form, champ, 'admin_rally_add.html', 'Add Rally')

    def get(self, champ_id):
        champ = models.Champ.get(champ_id)
        self.render_rally_form(admin_forms.RallyForm(instance=models.Rally(champ=champ)), champ)

    def post(self, champ_id):
        champ = models.Champ.get(champ_id)
        form = admin_forms.RallyForm(self.request.POST, instance=models.Rally(champ=champ))
        if form.is_valid():
            rally = form.save(commit=False)
            rally.import_image()
            rally.put()
            rally.generate_stages()
            context.flush_standings(champ)
            rally_id = str(rally.key())
            self.redirect(rally_url(rally_id))
        else:
            self.render_rally_form(form, champ)


class EditRallyHandler(webapp.RequestHandler):
    def render_form(self, form):
        self.response.out.write(template.render(
            settings.admin_template_path('admin_rally_edit.html'), create_props(self.request, {
                'title': 'Edit Rally',
                'form': form,
                'champ': form.instance.champ,
                'stages': form.instance.stages(),
                'image_width': models.RALLY_IMAGE_WIDTH,
            })))

    def get(self, rally_id):
        self.render_form(admin_forms.RallyForm(instance=models.Rally.get(rally_id)))

    def post(self, rally_id):
        form = admin_forms.RallyForm(self.request.POST, instance=models.Rally.get(rally_id))
        if form.is_valid():
            rally = form.save(commit=False)
            rally.import_image()
            rally.put()
            rally.generate_stages()
            context.flush_rally_results(rally)
            self.redirect(champ_url(str(rally.champ.key())) + '#rallies')
        else:
            self.render_form(form)


class ImportFileRallyHandler(webapp.RequestHandler):
    def post(self, champ_id):
        champ = models.Champ.get(champ_id)
        data = self.request.POST.get('roadbook')
        rally = None
        if data is not None and data != '':
            rally = context.import_rally_csv(champ, data.file)
        if rally:
            self.redirect(rally_url(str(rally.key())))
        else:
            self.redirect(champ_url(champ_id) + '#rallies')


class ImportChampRallyHandler(webapp.RequestHandler):
    def post(self, champ_id):
        rallies_csv = self.request.POST.get('rallies')
        if rallies_csv:
            champ = models.Champ.get(champ_id)
            champ.import_rallies(rallies_csv)
        self.redirect(champ_url(champ_id) + '#rallies')


class ImportRallyHandler(webapp.RequestHandler):
    def render_form(self, champ, name='', error=None):
        self.response.out.write(template.render(
            settings.admin_template_path('admin_rally_import.html'), create_props(self.request, {
                'title': 'Import Rally',
                'champ': champ,
                'error': error,
                'name': name,
            })))

    def get(self, champ_id):
        self.render_form(models.Champ.get(champ_id))

    def post(self, champ_id):
        champ = models.Champ.get(champ_id)
        name = self.request.POST.get('name')
        if name:
            rally = champ.rally_set.filter('name = ', name).get()
            if not rally:
                rally = models.Rally(champ=champ, name=name)
            if context.import_rally(rally):
                rally_id = str(rally.key())
                self.redirect(rally_url(rally_id))
            else:
                self.render_form(champ, name=name, error='Roadbook not found')
        else:
            self.render_form(champ, error='Please enter a rally name')


class RallyImageHandler(webapp.RequestHandler):
    def get(self, rally_id):
        rally = models.Rally.get(rally_id)
        if rally.image_data:
            self.response.headers['Content-Type'] = models.RALLY_IMAGE_CONTENT_TYPE
            self.response.out.write(rally.image_data)


class RallyImageImportHandler(webapp.RequestHandler):
    def post(self, rally_id):
        rally = models.Rally.get(rally_id)
        data = self.request.POST.get('image')
        if data is not None:
            if rally.import_image(data.file.read()):
                rally.image_link = None
                rally.put()
        else:
            # clear image
            rally.image_data = None
            rally.put()
        self.redirect(rally_url(rally_id))


class ReimportRallyHandler(webapp.RequestHandler):
    def post(self, rally_id):
        rally = models.Rally.get(rally_id)
        context.import_rally(rally)
        self.redirect(rally_url(rally_id))


class ReimportStagesHandler(webapp.RequestHandler):
    def post(self, rally_id):
        rally = models.Rally.get(rally_id)
        context.reimport_rally(rally)
        self.redirect(rally_url(rally_id))


class ImportRallyOverallResultsHandler(webapp.RequestHandler):
    def post(self, rally_id):
        rally = models.Rally.get(rally_id)
        ctx = context.Context(rally.champ, rally)
        ctx.import_results()
        self.redirect(rally_url(rally_id))


class ImportOfficialRallyOverallResultsHandler(webapp.RequestHandler):
    def post(self, rally_id):
        rally = models.Rally.get(rally_id)
        context.reimport_overall_results(rally)
        self.redirect(rally_url(rally_id))


class ClearRallyResultsHandler(webapp.RequestHandler):
    def post(self, rally_id):
        rally = models.Rally.get(rally_id)
        rally.clear_results()
        self.redirect(rally_url(rally_id))


class ClearRallyOverallResultsHandler(webapp.RequestHandler):
    def post(self, rally_id):
        rally = models.Rally.get(rally_id)
        rally.clear_overall_results()
        self.redirect(rally_url(rally_id))


class DeleteRallyDriverResultsHandler(webapp.RequestHandler):
    def post(self, rally_id):
        rally = models.Rally.get(rally_id)
        plugin_id = self.request.POST.get('plugin_id')
        if plugin_id:
            plugin_ids = context.parse_plugin_ids(plugin_id)
            rally.delete_driver_results(plugin_ids)
            context.flush_rally_results(rally)
        self.redirect(rally_url(rally_id))


class DeleteRallyHandler(AdminFormHandler):
    def get(self, rally_id):
        rally = models.Rally.get(rally_id)
        self.response.out.write(template.render(
            settings.admin_template_path('admin_generic_delete.html'), create_props(self.request, {
                'title': 'Admin Console - Delete Rally',
                'name': rally.name,
                'cancel_url': rally_url(str(rally.key())),
            })))

    def post(self, rally_id):
        rally = models.Rally.get(rally_id)
        if rally:
            champ_id = str(rally.champ.key())
            rally.delete()
            self.redirect(champ_url(champ_id) + '#rallies')
        else:
            self.redirect(rally_url(rally_id))


class FlushRallyResultsHandler(webapp.RequestHandler):
    def get(self, rally_id):
        rally = models.Rally.get(rally_id)
        context.flush_rally_results(rally)
        self.redirect(rally_url(rally_id))


class EditStageHandler(webapp.RequestHandler):
    def render_form(self, form):
        self.response.out.write(template.render(
            settings.admin_template_path('admin_stage_edit.html'), create_props(self.request, {
                'title': 'Edit Stage',
                'form': form,
                'rally': form.instance.rally,
                'champ': form.instance.rally.champ,
            })))

    def get(self, stage_id):
        self.render_form(admin_forms.StageForm(instance=models.Stage.get(stage_id)))

    def post(self, stage_id):
        stage = models.Stage.get(stage_id)
        form = admin_forms.StageForm(self.request.POST, instance=stage)
        if form.is_valid():
            form.save()
            self.redirect(rally_url(str(stage.rally.key())))
        else:
            self.render_form(form)


class ClearStageResultsHandler(webapp.RequestHandler):
    def post(self, stage_id):
        stage = models.Stage.get(stage_id)
        stage.clear_results()
        self.redirect(rally_url(str(stage.rally.key())) + '#stages')


class ImportStageResultsHandler(webapp.RequestHandler):
    def post(self, stage_id):
        stage = models.Stage.get(stage_id)
        ctx = context.Context(stage.rally.champ, stage.rally, stage.nr)
        ctx.import_results()
        self.redirect(rally_url(str(stage.rally.key())) + '#stages')


class AddBlacklistedDriverHandler(webapp.RequestHandler):
    def post(self, champ_id):
        plugin_id = self.request.POST.get('plugin_id')
        if plugin_id:
            plugin_ids = context.parse_plugin_ids(plugin_id)
            champ = models.Champ.get(champ_id)
            champ.blacklist(plugin_ids)
            context.flush_champ_results(champ)
        self.redirect(champ_url(champ_id) + '#blacklisted_drivers')


class DeleteBlacklistedDriverHandler(webapp.RequestHandler):
    def get(self, blacklisted_driver_id):
        blacklisted_driver = models.BlacklistedDriver.get(blacklisted_driver_id)
        champ_id = None
        if blacklisted_driver:
            champ = blacklisted_driver.champ
            champ_id = str(champ.key())
            blacklisted_driver.delete()
            context.flush_champ_results(champ)
        self.redirect(champ_url(champ_id) + '#blacklisted_drivers')


class FlushChampResultsHandler(webapp.RequestHandler):
    def get(self, champ_id):
        champ = models.Champ.get(champ_id)
        context.flush_champ_results(champ)
        self.redirect(champ_url(champ_id))


class AddDriverHandler(ChampSubFormHandler):
    def render_driver_form(self, form, champ):
        super(AddDriverHandler, self).render_form(form, champ, 'admin_driver_add.html', 'Add Driver')

    def get(self, champ_id):
        champ = models.Champ.get(champ_id)
        self.render_driver_form(admin_forms.DriverForm(), champ)

    def post(self, champ_id):
        champ = models.Champ.get(champ_id)
        form = admin_forms.DriverForm(self.request.POST, instance=models.Driver(champ=champ))
        if form.is_valid():
            form.save()
            context.flush_champ_results(champ, form.instance.start_date)
            self.redirect(champ_url(champ_id) + '#drivers')
        else:
            self.render_driver_form(form, champ)


class GenerateDriversHandler(webapp.RequestHandler):
    def post(self, champ_id):
        champ = models.Champ.get(champ_id)
        champ.generate_participants()
        self.redirect(champ_url(champ_id) + '#drivers')


class EditDriverHandler(ChampSubFormHandler):
    def render_driver_form(self, form):
        super(EditDriverHandler, self).render_form(form, form.instance.champ, 'admin_driver_edit.html', 'Edit Driver')

    def get(self, driver_id):
        self.render_driver_form(admin_forms.DriverForm(instance=models.Driver.get(driver_id)))

    def post(self, driver_id):
        driver = models.Driver.get(driver_id)
        form = admin_forms.DriverForm(self.request.POST, instance=driver)
        if form.is_valid():
            form.save()
            self.redirect(champ_url(str(driver.champ.key())) + '#drivers')
        else:
            self.render_driver_form(form)


class DeleteDriverHandler(AdminFormHandler):
    def get(self, driver_id):
        driver = models.Driver.get(driver_id)
        self.response.out.write(template.render(
            settings.admin_template_path('admin_generic_delete.html'), create_props(self.request, {
                'title': 'Admin Console - Delete Driver',
                'name': unicode(driver),
                'cancel_url': obj_url(str(driver.key()), 'driver'),
            })))

    def post(self, driver_id):
        driver = models.Driver.get(driver_id)
        champ_id = None
        if driver:
            champ_id = str(driver.champ.key())
            context.flush_champ_results(driver.champ, driver.start_date)
            driver.delete()
        self.redirect(champ_url(champ_id) + '#drivers')


class NewsItemListHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write(template.render(
            settings.admin_template_path('admin_news_list.html'), create_props(self.request, {
                'title': 'Admin Console - News Item List',
                'news_items': models.NewsItem.all().order('date'),
            })))


class AddNewsItemHandler(AdminFormHandler):
    def render_news_item_form(self, form):
        super(AddNewsItemHandler, self).render_form(form, 'admin_generic_add.html', 'Add News Item', 'news/')

    def get(self):
        self.render_news_item_form(admin_forms.NewsItemForm())

    def post(self):
        form = admin_forms.NewsItemForm(self.request.POST)
        if form.is_valid():
            form.save()
            self.redirect(news_url())
        else:
            self.render_news_item_form(form)


class EditNewsItemHandler(AdminFormHandler):
    def render_news_item_form(self, form):
        super(EditNewsItemHandler, self).render_form(form, 'admin_generic_edit.html', 'Edit News Item', 'news/')

    def get(self, news_item_id):
        self.render_news_item_form(admin_forms.NewsItemForm(instance=models.NewsItem.get(news_item_id)))

    def post(self, news_item_id):
        form = admin_forms.NewsItemForm(self.request.POST, instance=models.NewsItem.get(news_item_id))
        if form.is_valid():
            form.save()
            self.redirect(news_url())
        else:
            self.render_news_item_form(form)


class DeleteNewsItemHandler(webapp.RequestHandler):
    def get(self, news_item_id):
        news_item = models.NewsItem.get(news_item_id)
        self.response.out.write(template.render(
            settings.admin_template_path('admin_generic_delete.html'), create_props(self.request, {
                'title': 'Admin Console - Delete News Item',
                'name': news_item.title if news_item.title else str(news_item.key()),
                'cancel_url': news_url(str(news_item.key())),
            })))

    def post(self, news_item_id):
        news_item = models.NewsItem.get(news_item_id)
        if news_item:
            news_item.delete()
        self.redirect(news_url())
