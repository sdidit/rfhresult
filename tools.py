import itertools
import math


def grouper(n, iterable, fillvalue=None):
    """Collect data into fixed-length chunks or blocks
    grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"""
    args = [iter(iterable)] * n
    return itertools.izip_longest(fillvalue=fillvalue, *args)


def first(predicate, seq):
    """Get first element from seq that meets predicate, or None if no element meets the predicate"""
    for e in itertools.ifilter(predicate, seq):
        return e
    return None


def round_up(f):
    """round up and return integer"""
    return int(round(math.ceil(f)))


def signum(n):
    if n < 0:
        return -1
    if n > 0:
        return 1
    return 0


def accumulate(r):
    result = []
    cumulative = 0
    for x in r:
        cumulative += x
        result.append(cumulative)
    return result
