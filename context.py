import functools
import itertools

from google.appengine.api import memcache
from google.appengine.api import mail

import plugin.rsrbr_2014
import plugin.rsrbr_2015
import models
import settings
import time_util
import tools
import charts


PLUGINS = {
    # 'RSRBR_2010': plugin.rsrbr_2010,
    # 'RSRBR_2011': plugin.rsrbr_2011,
    # 'RSRBR_2012': plugin.rsrbr_2012,
    # 'RSRBR_2013': plugin.rsrbr_2013,
    'RSRBR_2014': plugin.rsrbr_2014,
    'RSRBR_2015': plugin.rsrbr_2015,
    # 'RBRTM_2011': plugin.rbrtm_2011,
    # 'RSFHU_2012': plugin.rsfhu_2012
}


class Points(object):
    def __init__(self, nr):
        self.points = 0
        self.pos = 0
        self.relevant = False
        self.nr = nr
        self.present = False

    def effective_points(self):
        return self.points if self.relevant else 0

    def is_podium(self):
        return 1 <= self.pos <= 3

    def __cmp__(self, p):
        return self.points - p.points or self.nr - p.nr


# all championship points for one driver, including total and position in standings
class DriverStandings(object):
    def __init__(self, plugin_id, rally_count):
        self.plugin_id = plugin_id
        self.rally_points = [Points(p) for p in range(rally_count)]
        self.total = 0
        self.pos = 0
        self.name = ''
        self.member_name = ''
        self.country = ''
        self.team = ''
        self.same_as_previous = False

    def pos_str(self):
        return '%d%s' % (self.pos, '=' if self.same_as_previous else '.')

    def is_driver(self, driver):
        return self.plugin_id == driver.plugin_id

    def cumulative_totals(self):
        return tools.accumulate(p.effective_points() for p in self.rally_points)

    def podium_finishes(self):
        return len(filter(Points.is_podium, self.rally_points))

    def __cmp__(self, ds):
        c = self.total - ds.total
        if c:
            return c

        # if points are equal, sort on number of podium finishes
        c = self.podium_finishes() - ds.podium_finishes()
        if c:
            return c

        # TODO: if number of podium finishes are equal, sort on number of scratches

        plist1 = reversed(self.rally_points)
        plist2 = reversed(ds.rally_points)
        for p1, p2 in itertools.izip(plist1, plist2):
            c = p1.points - p2.points
            if c:
                return c
        return 0


def flush_team_standings(team=None):
    ctx = Context()
    ctx.flush_team_cache()


def flush_standings(champ):
    ctx = Context(champ)
    ctx.flush_cache()


def flush_champ_results(champ, start_date=None):
    rallies = champ.rally_set
    if start_date is not None:
        rallies = rallies.filter('end_date >=', start_date)
    for rally in rallies:
        ctx = Context(champ, rally)
        ctx.flush_cache(True)


def flush_rally_results(rally):
    ctx = Context(rally.champ, rally)
    ctx.flush_cache(True)


def import_rally_csv(champ, f):
    roadbook = PLUGINS[champ.get_plugin_name()].parse_roadbook_csv(f)
    return roadbook and champ.import_roadbook(roadbook)


def roadbook_names():
    key = 'roadbook_names'
    value = memcache.get(key)
    if value is None:
        value = plugin.rsrbr_2015.roadbook_names()
        memcache.add(key, value, time_util.SECS_IN_HOUR)
    return value


def get_roadbook(rally):
    if isinstance(rally, models.Rally):
        return rally.get_roadbook() if rally.is_roadbook_available() else None
    return plugin.rsrbr_2015.roadbook(rally)


def csv_roadbook(rally):
    roadbook = get_roadbook(rally)
    return roadbook and roadbook.csv_values()


def import_rally(rally):
    name = rally.get_import_name()
    if rally.champ.is_public():
        roadbook = PLUGINS[rally.get_plugin_name()].roadbook_public(name, rally.session_count)
    else:
        roadbook = PLUGINS[rally.get_plugin_name()].roadbook(name)
    if not roadbook:
        return False
    rally.import_roadbook(roadbook)
    return True


def import_rallies():
    for rally in models.Champ.rallies_to_be_imported():
        import_rally(rally)


def reimport_rally(rally):
    if not rally.champ.is_public():
        roadbook = PLUGINS[rally.get_plugin_name()].roadbook2(rally.get_import_name(), rally.leg_count)
        if not roadbook:
            return False
        rally.import_stages(roadbook.stages)
    return True


def perform_race_checks(sender, mail_body, host_url):
    """
    Send a mail to member who have to send in their replays.

    Args:
        sender: email address of sender
        mail_body: will be formatted with (member name, rally name, url to plugin site)
    """
    race_checks = plugin.rsrbr_2015.race_checks()
    url, checks = race_checks
    if checks:
        url = '%s/%s' % (host_url, url)
        members = models.Member.all().filter('rsrbr_id !=', '')
        if members:
            member_map = dict([(member.rsrbr_id, member) for member in members])
            for check in checks:
                rally, nrs = check
                for nr in nrs:
                    member = member_map.get(nr)
                    if member and member.wants_notifications():
                        n = models.Notification(ntype='REPLAY_REMINDER',
                                                recipient=member.email_address,
                                                rally_name=rally)
                        ns = models.Notification.all()
                        ns = ns.filter('ntype =', n.ntype)
                        ns = ns.filter('recipient =', n.email_address)
                        ns = ns.filter('rally_name =', n.rally_name)
                        if not ns.fetch(1):
                            mail.send_mail(sender=sender,
                                           to="%s <%s>" % (member, member.email_address),
                                           subject="Replays opsturen voor %s" % rally,
                                           body=mail_body % (member, rally, url),
                                           bcc=sender)
                            n.put()


def reimport_overall_results(rally):
    results = PLUGINS[rally.get_plugin_name()].official_results(rally.get_import_name())
    if results:
        rally.reimport_overall_results(results)
        ctx = Context(rally.champ, rally)
        ctx.flush_cache()


def parse_plugin_ids(ids):
    return [pid.strip(',;/|-+_') for pid in ids.split()]


def collect_news(past_days=100, max_items=5):
    def make_team_change_body(t):
        return u'{} {} {}'.format(t['member'].name, u'joined' if t['joined'] else u'left', t['team'].name)

    today = time_util.today()
    from_date = time_util.date_offset(today, -past_days)
    team_changes = models.TeamMember.team_changes(from_date, today)
    news = [{'date': t['date'], 'body': make_team_change_body(t)} for t in team_changes]
    items = models.NewsItem.latest(from_date, today)
    news += map(models.NewsItem.make_entry, items)
    news.sort(key=lambda m: m['date'], reverse=True)
    if max_items > 0:
        news = news[:max_items]
    return news


class Event(object):
    def __init__(self, start_date, rally=None):
        self.start_date = start_date
        self.rally = rally
        today = time_util.today()
        self.end_date = time_util.date_offset(self.start_date, 6)
        self.past = today > self.end_date
        self.future = today < self.start_date
        self.count = 1  # for rowspan

    @staticmethod
    def collect_events():
        since = time_util.iso_year_start(settings.YEAR)
        events = [Event(rally.start_date, rally) for rally in models.Rally.all_rallies_sortby_date(since)]
        if len(events) > 1:
            # insert empty events
            i = 1
            while i < len(events):
                date = time_util.date_offset(events[i - 1].start_date, 7)
                if date < events[i].start_date:
                    events.insert(i, Event(date))
                i += 1
            # set number of events in same week
            event = events[0]
            i = 1
            while i < len(events):
                if event.start_date == events[i].start_date:
                    event.count += 1
                    events[i].count = 0
                else:
                    event = events[i]
                i += 1
        return events

    def __str__(self):
        return unicode(self).encode(settings.DEFAULT_CHARSET)

    def __unicode__(self):
        return self.rally.name if self.rally else self.sdate_str()

    def sdate_str(self):
        return time_util.format_date(self.start_date)

    def edate_str(self):
        return time_util.format_date(self.end_date)

    def sdate_str_short(self):
        return time_util.format_date_short(self.start_date)

    def edate_str_short(self):
        return time_util.format_date_short(self.end_date)

    def edate_str_short(self):
        return time_util.format_date_short(self.end_date)

    def sdate_str_iso(self):
        return time_util.format_date_iso(self.start_date)

    def edate_str_iso(self):
        return time_util.format_date_iso(time_util.date_offset(self.end_date, 1))

    def week_str(self):
        return '%02d' % time_util.date_to_iso_week(self.start_date)


class Context(object):
    def __init__(self, champ=None, rally=None, stage=0, cls='', flt=0):
        assert champ is None or isinstance(champ, models.Champ)
        assert rally is None or isinstance(rally, models.Rally)
        self.champ = champ
        self.rally = rally
        self.stage = stage
        self.cls = cls
        self.flt = flt

    def __str__(self):
        return 'stage %s, class %s, filter %s' % (self.stage, self.cls, self.flt)

    def props(self):
        # class and filter are globally available, even if not used
        p = {}
        p['class'] = self.cls
        p['filter'] = self.flt
        p['rfh'] = self.is_filtered()
        if self.champ is not None:
            p['champ'] = self.champ
            if self.rally is not None:
                p['rally'] = self.rally
                p['stage'] = self.stage
                p['prevstage'] = self.prev_stage()
                p['nextstage'] = self.next_stage()
                if self.stage:
                    p['stage_obj'] = self.get_stage_obj()
        return p

    def is_overall(self):
        return self.stage == 0

    def is_filtered(self):
        return self.flt != 0 or self.champ is not None and not self.champ.is_show_filter()

    def is_all_classes(self):
        return not self.cls

    def prev_stage(self):
        return self.stage - 1 if self.stage > 1 else 0

    def next_stage(self):
        return self.stage + 1 if self.stage < self.rally.stage_count else 0

    def get_stage_obj(self):
        return self.rally.get_stage(self.stage)

    def make_results_key(self, stage=None, cls=None, flt=None):
        prefix = 'results'
        if stage is None:
            stage = self.stage
        if cls is None:
            cls = self.cls
        if flt is None:
            flt = self.flt
        if cls:
            return '%s_%s_%s_%s_%s' % (flt, prefix, self.rally.key(), cls, stage)
        return '%s_%s_%s_%s' % (flt, prefix, self.rally.key(), stage)

    def make_team_results_key(self):
        return 'team_results_%s' % self.rally.key()

    def make_live_results_key(self):
        return 'liveresults_%s_%s' % (self.rally.key(), self.stage)

    def make_standings_key(self, cls=None, flt=None):
        prefix = 'standings'
        if cls is None:
            cls = self.cls
        if flt is None:
            flt = self.flt
        if cls:
            return '%s_%s_%s_%s' % (flt, prefix, self.champ.key(), cls)
        return '%s_%s_%s' % (flt, prefix, self.champ.key())

    def make_teamcup_standings_key(self):
        return 'cup_standings'

    def flush_cache(self, all_stages=False):
        keys = [self.make_teamcup_standings_key()]
        if self.champ:
            classes = [cls for cls in self.champ.result_class_names()]
            classes.append('')
            for cls in classes:
                for f in models.ALL_FILTERS:
                    flt = f['value']
                    keys.append(self.make_standings_key(cls=cls, flt=flt))
                    if self.rally:
                        if all_stages:
                            for stage in range(0, self.rally.stage_count + 1):
                                keys.append(self.make_results_key(stage=stage, cls=cls, flt=flt))
                        else:
                            keys.append(self.make_results_key(cls=cls, flt=flt))
                            if not self.is_overall():
                                # for stage also clear overall result
                                keys.append(self.make_results_key(stage=0, cls=cls, flt=flt))
            self.champ.delete_standings()
            if self.rally:
                keys.append(self.make_team_results_key())
                self.rally.delete_results_cache()
        memcache.delete_multi(keys)

    def flush_team_cache(self):
        keys = [self.make_teamcup_standings_key()]
        for teamcup in models.TeamCup.all():
            for champ in teamcup.champ_set:
                self.champ = champ
                for rally in champ.rally_set:
                    self.rally = rally
                    keys.append(self.make_team_results_key())
        memcache.delete_multi(keys)

    def driver_pos_str(self, driver):
        self.champ = driver.champ
        ds = self.driver_standings(driver)
        self.champ = None
        return ds and ds.pos_str() or '-'

    def driver_standings(self, driver):
        standings = self.cached_standings()
        is_driver = functools.partial(DriverStandings.is_driver, driver=driver)
        return tools.first(is_driver, standings)

    def cached_team_total_standings(self):
        key = self.make_teamcup_standings_key()
        standings = memcache.get(key)
        if standings is None:
            cup_standings = self.team_standings()
            standings = cup_standings[0].standings if cup_standings else []
            memcache.add(key, standings)
        return standings

    def top_n_teamcup_standings(self, n):
        standings = self.cached_team_total_standings()
        if n:
            standings = standings[:n]
        return [{'pos_str': ts.pos_str, 'name': ts.team.name, 'points': int(round(ts.points))} for ts in standings]

    def top_three_standings(self):
        return self.top_n_standings(3)

    def top_n_standings(self, n):
        standings = self.cached_standings()
        if n:
            standings = standings[:n]
        return [{'pos': ds.pos_str(),
                 'name': ds.name,
                 'member_name': ds.member_name if hasattr(ds, 'member_name') else '',
                 'team': ds.team,
                 'country': ds.country,
                 'points': str(ds.total)} for ds in standings]

    def standings_bar_chart(self):
        items = map(lambda ds: charts.ChartItem(ds.name, (ds.total,)), self.cached_standings()[:10])
        return charts.create_bar_chart(items, tick_round=-1)

    def standings_line_chart(self):
        items = map(lambda ds: charts.ChartItem(ds.name, ds.cumulative_totals()), self.cached_standings()[:10])
        labels = [rally.short_name or '' for rally in self.champ.rallies()]
        return charts.create_line_chart(items, labels)

    def cached_standings(self):
        key = self.make_standings_key()
        value = memcache.get(key)
        if value is None:
            value = self.champ.get_standings(self.cls, self.flt)
            if value is None:
                value = self.standings()
                if value:
                    self.champ.set_standings(value, self.cls, self.flt)
            if value is not None:
                memcache.add(key, value)
        return value

    def standings(self):
        rallies = self.champ.rallies()
        count = rallies.count()
        standings = {}
        self.stage = 0

        # calculate driver points for each rally
        for r, rally in enumerate(rallies):
            assert isinstance(rally, models.Rally)
            if rally.are_results_imported():
                self.rally = rally
                results = self.cached_results()
                for result in results:
                    assert isinstance(result, models.Result)
                    plugin_id = result.plugin_id
                    ds = standings.get(plugin_id)
                    if ds is None:
                        ds = DriverStandings(plugin_id, count)
                        ds.name = result.name
                        if hasattr(result, 'member_name'):  # workaround for cached results without member_name
                            ds.member_name = result.member_name
                        ds.country = result.country
                        ds.team = result.team
                        standings[plugin_id] = ds
                    rally_points = ds.rally_points[r]
                    rally_points.points = result.total_points()
                    rally_points.pos = result.pos
                    rally_points.present = True     # driver was there
        self.rally = None

        # calculate total points, including the best relevant rally results only
        # total points are appended to the end of the rally points list
        relevant_rally_count = min(count,
                                   self.champ.relevant_rally_count) if self.champ.relevant_rally_count > 0 else count
        for ds in standings.values():
            rally_points = sorted(ds.rally_points, reverse=True)
            rally_points = rally_points[:relevant_rally_count]
            total_points = 0
            for points in rally_points:
                total_points += points.points
                points.relevant = True    # mark as relevant
            ds.total = total_points

        # sort on drivers with most total points
        # if total is equal, rally results from last to first will decide order
        sorted_standings = sorted(standings.values(), reverse=True)
        return self._calc_standing_positions(sorted_standings)

    def _calc_standing_positions(self, standings):
        prev_points = -1
        pos = 1
        for i, ds in enumerate(standings):
            points = ds.total
            ds.same_as_previous = points == prev_points
            if not ds.same_as_previous:
                pos = i + 1
            ds.pos = pos
            prev_points = points
        return standings

    def team_standings(self):
        self.flt = 1
        self.cls = None
        # cannot cache because value is too large (almost 500%)
        value = map(lambda teamcup: self._calc_team_standings(teamcup), models.TeamCup.all())
        return value

    def team_standings_with_charts(self):
        value = self.team_standings()
        for teamcup in value:
            teamcup.create_bar_chart()
            teamcup.create_line_chart()
        return value

    def _calc_team_standings(self, teamcup):
        team_standings = {}
        # list of rallies of the championships in the team cup, sorted by start date
        rallies = sorted((rally for champ in teamcup.champ_set for rally in champ.rally_set),
                         key=lambda r: r.start_date)
        count = len(rallies)
        for index, rally in enumerate(rallies):
            self.champ = rally.champ
            self.rally = rally
            team_results = self.team_results()
            for team_result in team_results:
                key = team_result.team.key
                team_standing = team_standings.get(key)
                if team_standing is None:
                    team_standings[key] = team_standing = models.TeamStanding(team_result.team, count)
                team_standing.add(team_result, index)
        teamcup.init_standings(self.process_team_results(team_standings.values()), rallies)
        return teamcup

    def csv_results(self):
        self.stage = 0
        results = self.cached_results() or ()
        return map(models.Result.csv_values, results)

    def cached_results(self):
        key = self.make_results_key()
        value = memcache.get(key)
        if value is None:
            if self.is_overall():
                value = self.rally.get_overall_results(self.cls, self.flt)
                if value is None:
                    value = self.results()
                    if value is not None:
                        self.rally.set_overall_results(value, self.cls, self.flt)
            else:
                value = self.results()
            if value is not None:
                memcache.add(key, value)
        return value

    def team_results(self):
        if self.is_overall() and self.is_filtered() and self.is_all_classes():
            key = self.make_team_results_key()
            value = memcache.get(key)
            if value is None:
                team_cup = self.champ.team_cup
                if team_cup and self.rally.is_finished():
                    bonus = team_cup.get_bonus()
                    teams = models.Team.all()
                    raw_team_results = map(lambda team: self.calc_team_result(team, bonus), teams)
                    value = self.process_team_results(raw_team_results)
                else:
                    value = []
                memcache.add(key, value)
        else:
            value = []
        return value

    def calc_team_result(self, team, bonus=None):
        results = self.cached_results()
        key = team.named_key()
        if not results:
            return models.TeamResult(key)
        driver_map = team.driver_map(self.rally)
        driver_results = filter(lambda r: r.plugin_id in driver_map, results)
        return models.TeamResult(key, driver_results, driver_map, bonus)

    def process_team_results(self, raw_team_results):
        filtered_team_results = filter(models.TeamResult.has_points, raw_team_results)
        sorted_team_results = sorted(filtered_team_results)
        for i, v in enumerate(sorted_team_results):
            v.pos_str = "%d." % (i + 1)
        return sorted_team_results

    def import_results(self):
        raw_results = PLUGINS[self.champ.get_plugin_name()].results(self.rally, self.stage)
        if raw_results:
            # do not import before rally is finished
            if self.rally.is_finished():
                self.rally.import_results(self.stage, raw_results)
                self.flush_cache()

    def results(self):
        if not self.rally.are_results_imported(self.stage):
            return None
        raw_results = self.rally.get_results(self.stage)
        results = self._process_results(raw_results)
        if self.is_overall():
            self._calc_bonus_points(results)
        return results

    def full_results_url(self):
        return self.champ.is_unofficial() and PLUGINS[self.champ.get_plugin_name()].full_results_url(
            self.rally.get_import_name(), self.stage)

    def live_results(self):
        if self.rally.are_live_results_available():
            raw_results = PLUGINS[self.champ.get_plugin_name()].live_results(self.rally, self.stage)
            # cache last known live result
            key = self.make_live_results_key()
            if raw_results:
                memcache.add(key, raw_results, time_util.SECS_IN_DAY)
            else:
                raw_results = memcache.get(key)
                if raw_results is None:
                    raw_results = ()
            return self._process_results(raw_results)
        return ()

    def _calc_bonus_points(self, results):
        # calculate number of wins and add bonus points
        assert self.is_overall()
        presence_bonus = self.rally.champ.bonus_for_start or 0
        finish_bonus = self.rally.champ.bonus_for_finish or 0
        most_wins_bonus = self.rally.champ.bonus_for_most_wins or 0

        driver_map, most_wins = self._build_stage_result_map()
        most_win_count = most_wins[0][1] if most_wins else -1
        most_wins = dict(most_wins)  # pid -> win count

        for result in results:
            result.presence_bonus = presence_bonus
            pid = result.plugin_id
            dr = driver_map.get(pid)
            if dr:
                finished_results = filter(models.Result.is_finished, dr.values())
                result.finishes = len(finished_results)
                if result.finishes == self.rally.stage_count:
                    result.finish_bonus = finish_bonus
                elif self.champ.is_public():  # for public sessions driver is retired if not all stages are finished
                    result.status = models.STATE_RETIRED  # doesn't affect rank, only points!
            driver_wins = most_wins.get(pid)
            if driver_wins is not None:
                result.wins = driver_wins
                if driver_wins == most_win_count:
                    result.most_wins_bonus = most_wins_bonus

    def _build_stage_result_map(self):
        driver_map = {}
        wins = {}
        for ss in range(1, self.rally.stage_count + 1):
            self.stage = ss
            # prevent importing all stages, because it will take too long and fail
            if self.rally.are_results_imported(ss):
                results = self.cached_results()
                if results:
                    # track number of wins
                    first_result = results[0]
                    pid = first_result.plugin_id
                    cur = wins.get(pid)
                    if cur is None:
                        wins[pid] = [pid, 1]
                    else:
                        cur[1] += 1
                        # track stage result of each driver
                    for result in results:
                        pid = result.plugin_id
                        dr = driver_map.get(pid)
                        if dr is None:
                            driver_map[pid] = dr = {}
                        dr[ss] = result

        self.stage = 0
        most_wins = sorted(wins.values(), lambda a, b: b[1] - a[1])
        return driver_map, most_wins

    def _process_results(self, raw_results):
        return self.rally.process_results(raw_results, self.flt, self.cls)
