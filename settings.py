import os

DEBUG = os.environ['SERVER_SOFTWARE'].startswith('Dev')
TEMPLATE_DEBUG = DEBUG

CURRENT_VERSION_ID = os.environ['CURRENT_VERSION_ID']
ADMIN_EMAIL = 'sdidit@gmail.com'
DEFAULT_CHARSET = 'iso-8859-1'

ROOT = os.path.dirname(__file__)
STATIC_ROOT = os.path.join(ROOT, 'static')
TEMPLATE_ROOT = os.path.join(ROOT, 'templates')

YEAR = 2015

# 10 years...
MAX_AGE_NEWS = 10 * 365


def template_path(tmpl):
    return os.path.join(TEMPLATE_ROOT, tmpl)


def admin_template_path(tmpl):
    return os.path.join(TEMPLATE_ROOT, tmpl)


def shell_template_path(tmpl):
    return os.path.join(TEMPLATE_ROOT, tmpl)
